#-------------------------------------------------
#
# Project created by QtCreator 2013-04-28T14:18:16
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = IO2013Uj
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    managerwindow.cpp

HEADERS  += mainwindow.h \
    managerwindow.h

FORMS    += \
    mainwindow.ui \
    managerwindow.ui
