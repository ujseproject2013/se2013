/********************************************************************************
** Form generated from reading UI file 'managerwindow.ui'
**
** Created by: Qt User Interface Compiler version 5.0.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MANAGERWINDOW_H
#define UI_MANAGERWINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QListView>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenu>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QPlainTextEdit>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QAction *actionPo_cz_z_baz;
    QAction *actionZamknij;
    QAction *actionRoz_cz_baz;
    QWidget *centralwidget;
    QListView *zamowienia;
    QPushButton *pushButton;
    QPushButton *pushButton_2;
    QPushButton *pushButton_3;
    QPlainTextEdit *plainTextEdit;
    QLabel *label;
    QPlainTextEdit *plainTextEdit_2;
    QLabel *label_2;
    QLabel *label_3;
    QLabel *label_4;
    QPlainTextEdit *plainTextEdit_3;
    QPlainTextEdit *plainTextEdit_4;
    QPlainTextEdit *plainTextEdit_5;
    QPlainTextEdit *plainTextEdit_6;
    QLabel *label_5;
    QLabel *label_6;
    QMenuBar *menubar;
    QMenu *menuMenu;
    QStatusBar *statusbar;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QStringLiteral("MainWindow"));
        MainWindow->setWindowModality(Qt::ApplicationModal);
        MainWindow->resize(800, 600);
        QSizePolicy sizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(MainWindow->sizePolicy().hasHeightForWidth());
        MainWindow->setSizePolicy(sizePolicy);
        QFont font;
        font.setFamily(QStringLiteral("OpenSymbol"));
        font.setBold(false);
        font.setItalic(false);
        font.setWeight(50);
        MainWindow->setFont(font);
        actionPo_cz_z_baz = new QAction(MainWindow);
        actionPo_cz_z_baz->setObjectName(QStringLiteral("actionPo_cz_z_baz"));
        actionZamknij = new QAction(MainWindow);
        actionZamknij->setObjectName(QStringLiteral("actionZamknij"));
        actionRoz_cz_baz = new QAction(MainWindow);
        actionRoz_cz_baz->setObjectName(QStringLiteral("actionRoz_cz_baz"));
        centralwidget = new QWidget(MainWindow);
        centralwidget->setObjectName(QStringLiteral("centralwidget"));
        zamowienia = new QListView(centralwidget);
        zamowienia->setObjectName(QStringLiteral("zamowienia"));
        zamowienia->setGeometry(QRect(10, 20, 411, 501));
        zamowienia->setAutoFillBackground(false);
        zamowienia->setFrameShadow(QFrame::Sunken);
        zamowienia->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOn);
        zamowienia->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
        zamowienia->setAutoScrollMargin(8);
        zamowienia->setEditTriggers(QAbstractItemView::CurrentChanged|QAbstractItemView::SelectedClicked);
        zamowienia->setTabKeyNavigation(true);
        zamowienia->setDefaultDropAction(Qt::MoveAction);
        zamowienia->setIconSize(QSize(250, 75));
        zamowienia->setProperty("isWrapping", QVariant(true));
        zamowienia->setGridSize(QSize(250, 75));
        pushButton = new QPushButton(centralwidget);
        pushButton->setObjectName(QStringLiteral("pushButton"));
        pushButton->setGeometry(QRect(440, 20, 341, 106));
        QSizePolicy sizePolicy1(QSizePolicy::MinimumExpanding, QSizePolicy::MinimumExpanding);
        sizePolicy1.setHorizontalStretch(0);
        sizePolicy1.setVerticalStretch(0);
        sizePolicy1.setHeightForWidth(pushButton->sizePolicy().hasHeightForWidth());
        pushButton->setSizePolicy(sizePolicy1);
        QIcon icon;
        icon.addFile(QStringLiteral("../Pobrane/refresh1.png"), QSize(), QIcon::Normal, QIcon::Off);
        pushButton->setIcon(icon);
        pushButton->setIconSize(QSize(96, 96));
        pushButton->setCheckable(false);
        pushButton_2 = new QPushButton(centralwidget);
        pushButton_2->setObjectName(QStringLiteral("pushButton_2"));
        pushButton_2->setGeometry(QRect(630, 180, 161, 111));
        pushButton_3 = new QPushButton(centralwidget);
        pushButton_3->setObjectName(QStringLiteral("pushButton_3"));
        pushButton_3->setGeometry(QRect(440, 180, 161, 111));
        pushButton_3->setAutoDefault(true);
        plainTextEdit = new QPlainTextEdit(centralwidget);
        plainTextEdit->setObjectName(QStringLiteral("plainTextEdit"));
        plainTextEdit->setGeometry(QRect(450, 330, 131, 31));
        plainTextEdit->setFocusPolicy(Qt::NoFocus);
        plainTextEdit->setContextMenuPolicy(Qt::NoContextMenu);
        plainTextEdit->setAcceptDrops(false);
        plainTextEdit->setAutoFillBackground(true);
        plainTextEdit->setInputMethodHints(Qt::ImhDigitsOnly);
        plainTextEdit->setUndoRedoEnabled(false);
        plainTextEdit->setLineWrapMode(QPlainTextEdit::WidgetWidth);
        plainTextEdit->setReadOnly(true);
        plainTextEdit->setOverwriteMode(false);
        plainTextEdit->setTextInteractionFlags(Qt::NoTextInteraction);
        plainTextEdit->setBackgroundVisible(false);
        plainTextEdit->setCenterOnScroll(false);
        label = new QLabel(centralwidget);
        label->setObjectName(QStringLiteral("label"));
        label->setGeometry(QRect(460, 310, 121, 20));
        plainTextEdit_2 = new QPlainTextEdit(centralwidget);
        plainTextEdit_2->setObjectName(QStringLiteral("plainTextEdit_2"));
        plainTextEdit_2->setGeometry(QRect(640, 330, 131, 31));
        plainTextEdit_2->setFocusPolicy(Qt::NoFocus);
        plainTextEdit_2->setContextMenuPolicy(Qt::NoContextMenu);
        plainTextEdit_2->setAcceptDrops(false);
        plainTextEdit_2->setAutoFillBackground(true);
        plainTextEdit_2->setInputMethodHints(Qt::ImhDigitsOnly);
        plainTextEdit_2->setUndoRedoEnabled(false);
        plainTextEdit_2->setLineWrapMode(QPlainTextEdit::WidgetWidth);
        plainTextEdit_2->setReadOnly(true);
        plainTextEdit_2->setOverwriteMode(false);
        plainTextEdit_2->setTextInteractionFlags(Qt::NoTextInteraction);
        plainTextEdit_2->setBackgroundVisible(false);
        plainTextEdit_2->setCenterOnScroll(false);
        label_2 = new QLabel(centralwidget);
        label_2->setObjectName(QStringLiteral("label_2"));
        label_2->setGeometry(QRect(630, 310, 151, 20));
        label_3 = new QLabel(centralwidget);
        label_3->setObjectName(QStringLiteral("label_3"));
        label_3->setGeometry(QRect(450, 390, 141, 20));
        label_4 = new QLabel(centralwidget);
        label_4->setObjectName(QStringLiteral("label_4"));
        label_4->setGeometry(QRect(630, 390, 151, 21));
        plainTextEdit_3 = new QPlainTextEdit(centralwidget);
        plainTextEdit_3->setObjectName(QStringLiteral("plainTextEdit_3"));
        plainTextEdit_3->setGeometry(QRect(450, 410, 131, 31));
        plainTextEdit_3->setFocusPolicy(Qt::NoFocus);
        plainTextEdit_3->setContextMenuPolicy(Qt::NoContextMenu);
        plainTextEdit_3->setAcceptDrops(false);
        plainTextEdit_3->setAutoFillBackground(true);
        plainTextEdit_3->setInputMethodHints(Qt::ImhDigitsOnly);
        plainTextEdit_3->setUndoRedoEnabled(false);
        plainTextEdit_3->setLineWrapMode(QPlainTextEdit::WidgetWidth);
        plainTextEdit_3->setReadOnly(true);
        plainTextEdit_3->setOverwriteMode(false);
        plainTextEdit_3->setTextInteractionFlags(Qt::NoTextInteraction);
        plainTextEdit_3->setBackgroundVisible(false);
        plainTextEdit_3->setCenterOnScroll(false);
        plainTextEdit_4 = new QPlainTextEdit(centralwidget);
        plainTextEdit_4->setObjectName(QStringLiteral("plainTextEdit_4"));
        plainTextEdit_4->setGeometry(QRect(640, 410, 131, 31));
        plainTextEdit_4->setFocusPolicy(Qt::NoFocus);
        plainTextEdit_4->setContextMenuPolicy(Qt::NoContextMenu);
        plainTextEdit_4->setAcceptDrops(false);
        plainTextEdit_4->setAutoFillBackground(true);
        plainTextEdit_4->setInputMethodHints(Qt::ImhDigitsOnly);
        plainTextEdit_4->setUndoRedoEnabled(false);
        plainTextEdit_4->setLineWrapMode(QPlainTextEdit::WidgetWidth);
        plainTextEdit_4->setReadOnly(true);
        plainTextEdit_4->setOverwriteMode(false);
        plainTextEdit_4->setTextInteractionFlags(Qt::NoTextInteraction);
        plainTextEdit_4->setBackgroundVisible(false);
        plainTextEdit_4->setCenterOnScroll(false);
        plainTextEdit_5 = new QPlainTextEdit(centralwidget);
        plainTextEdit_5->setObjectName(QStringLiteral("plainTextEdit_5"));
        plainTextEdit_5->setGeometry(QRect(640, 480, 131, 31));
        plainTextEdit_5->setFocusPolicy(Qt::NoFocus);
        plainTextEdit_5->setContextMenuPolicy(Qt::NoContextMenu);
        plainTextEdit_5->setAcceptDrops(false);
        plainTextEdit_5->setAutoFillBackground(true);
        plainTextEdit_5->setInputMethodHints(Qt::ImhDigitsOnly);
        plainTextEdit_5->setUndoRedoEnabled(false);
        plainTextEdit_5->setLineWrapMode(QPlainTextEdit::WidgetWidth);
        plainTextEdit_5->setReadOnly(true);
        plainTextEdit_5->setOverwriteMode(false);
        plainTextEdit_5->setTextInteractionFlags(Qt::NoTextInteraction);
        plainTextEdit_5->setBackgroundVisible(false);
        plainTextEdit_5->setCenterOnScroll(false);
        plainTextEdit_6 = new QPlainTextEdit(centralwidget);
        plainTextEdit_6->setObjectName(QStringLiteral("plainTextEdit_6"));
        plainTextEdit_6->setGeometry(QRect(450, 480, 131, 31));
        plainTextEdit_6->setFocusPolicy(Qt::NoFocus);
        plainTextEdit_6->setContextMenuPolicy(Qt::NoContextMenu);
        plainTextEdit_6->setAcceptDrops(false);
        plainTextEdit_6->setAutoFillBackground(true);
        plainTextEdit_6->setInputMethodHints(Qt::ImhDigitsOnly);
        plainTextEdit_6->setUndoRedoEnabled(false);
        plainTextEdit_6->setLineWrapMode(QPlainTextEdit::WidgetWidth);
        plainTextEdit_6->setReadOnly(true);
        plainTextEdit_6->setOverwriteMode(false);
        plainTextEdit_6->setTextInteractionFlags(Qt::NoTextInteraction);
        plainTextEdit_6->setBackgroundVisible(false);
        plainTextEdit_6->setCenterOnScroll(false);
        label_5 = new QLabel(centralwidget);
        label_5->setObjectName(QStringLiteral("label_5"));
        label_5->setGeometry(QRect(440, 460, 151, 21));
        label_6 = new QLabel(centralwidget);
        label_6->setObjectName(QStringLiteral("label_6"));
        label_6->setGeometry(QRect(620, 460, 171, 21));
        MainWindow->setCentralWidget(centralwidget);
        menubar = new QMenuBar(MainWindow);
        menubar->setObjectName(QStringLiteral("menubar"));
        menubar->setGeometry(QRect(0, 0, 800, 27));
        menuMenu = new QMenu(menubar);
        menuMenu->setObjectName(QStringLiteral("menuMenu"));
        MainWindow->setMenuBar(menubar);
        statusbar = new QStatusBar(MainWindow);
        statusbar->setObjectName(QStringLiteral("statusbar"));
        MainWindow->setStatusBar(statusbar);

        menubar->addAction(menuMenu->menuAction());
        menuMenu->addAction(actionPo_cz_z_baz);
        menuMenu->addAction(actionRoz_cz_baz);
        menuMenu->addSeparator();
        menuMenu->addAction(actionZamknij);

        retranslateUi(MainWindow);

        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QApplication::translate("MainWindow", "MainWindow", 0));
        actionPo_cz_z_baz->setText(QApplication::translate("MainWindow", "Po\305\202\304\205cz z baz\304\205", 0));
        actionZamknij->setText(QApplication::translate("MainWindow", "Zamknij", 0));
        actionRoz_cz_baz->setText(QApplication::translate("MainWindow", "Roz\305\202\304\205cz baz\304\231", 0));
        pushButton->setText(QApplication::translate("MainWindow", "Od\305\233wierz", 0));
        pushButton_2->setText(QApplication::translate("MainWindow", "Anuluj", 0));
        pushButton_3->setText(QApplication::translate("MainWindow", "Zatwierd\305\272", 0));
        label->setText(QApplication::translate("MainWindow", "<html><head/><body><p align=\"center\"><span style=\" font-size:10pt;\">\305\232rednia pensja</span></p></body></html>", 0));
        label_2->setText(QApplication::translate("MainWindow", "<html><head/><body><p align=\"center\"><span style=\" font-size:10pt;\">Liczba pracownik\303\263w</span></p></body></html>", 0));
        label_3->setText(QApplication::translate("MainWindow", "<html><head/><body><p align=\"center\"><span style=\" font-size:10pt;\">Najwy\305\274sza pensja</span></p></body></html>", 0));
        label_4->setText(QApplication::translate("MainWindow", "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:'OpenSymbol'; font-size:11pt; font-weight:400; font-style:normal;\">\n"
"<p align=\"center\" style=\" margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:10pt;\">Pracownicy na urlopie</span></p></body></html>", 0));
        label_5->setText(QApplication::translate("MainWindow", "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:'OpenSymbol'; font-size:11pt; font-weight:400; font-style:normal;\">\n"
"<p align=\"center\" style=\" margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:10pt;\">Najni\305\274sza pensja</span></p></body></html>", 0));
        label_6->setText(QApplication::translate("MainWindow", "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:'OpenSymbol'; font-size:11pt; font-weight:400; font-style:normal;\">\n"
"<p align=\"center\" style=\" margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:10pt;\">Ilo\305\233\304\207 pracuj\304\205cych obecnie</span></p></body></html>", 0));
        menuMenu->setTitle(QApplication::translate("MainWindow", "Menu", 0));
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MANAGERWINDOW_H
