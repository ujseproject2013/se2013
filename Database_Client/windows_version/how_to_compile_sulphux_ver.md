Aby skompilowa� w jakimkolwiek projekcie ( przyk�ad robiony  w Codeblocks ):

1. Do��cz folder include z katalogu PostgreSQL w "Build options" projektu
do sekcji 'Compiler' w karcie 'Search directores'.

2. Tak samo dodaj folder lib do sekcji 'Linker' w karcie 'Search directores'.

3. Przejd� do karty 'Linker settings' i dodaj tam biblioteke libpq.lib znajduje si� on w folderze lib, w PostgreSQL.

4. Ustaw dodatkowe flagi kompilacji na -lpq

5. Po zbudowaniu dodaj biblioteki dll do pliku exe, z katalogu 'bin' Postgresa ( nie wszystkie s� potrzebne ).

Uwaga: wszystkie opcje i �cie�ki musz� by� ustawione og�lnie dla Release i Debug.


