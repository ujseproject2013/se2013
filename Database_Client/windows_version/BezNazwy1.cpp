#include <iostream>
#include <libpq-fe.h>
#include <string.h>
#include <fstream>
#include <conio.h>
#include <cstdlib>
using namespace std;


//dane do połączenia
char pghost[] = "localhost";
char pgport[] = "5432";
char pgbase[] = "baza2";
char pgnew_base[] = "nowa.sql";
char pguser[] = "postgres";
char pgpass[] = "postgres";
char szkielet[] = "../sql/szkielet_bazy.sql";
char widoki[] = "../sql/widoki.sql";
char tigerry[] = "../sql/wyzwalacze.sql";
char funkcje[] = "../sql/funkcje.sql";
char dane[] = "../sql/dane_poczatkowe.sql";

/* ************************************************************************* */

void UtworzBaze(PGconn *conn){

     PGresult *result;

  // TWORZENIE Z DOMYSLNA NAZWA "pgbase[]=baza"
         char polecenie[] = "CREATE DATABASE ";
         strcat(polecenie, pgbase);

  // WYKONANIE POLECENIA
         result = PQexec(conn, polecenie);
         cout<<PQresStatus(PQresultStatus(result));
         cout<<"\n";
         cout<<PQresultErrorMessage(result);
         cout<<"\n";
         PQclear(result);
     }


/* ************************************************************************ */

void Rozlacz(PGconn *conn)
{

     if (PQstatus(conn) == CONNECTION_OK)
     {
        PQfinish(conn);
        cout<<"polaczenie zakonczone\n";
     }else{
           cout<<"nie masz aktywnego polaczenia\n";
           cout<<PQstatus(conn);
            }
}



/* ************************************************************************ */

void Usun(PGconn *conn){
     PGresult *result;

     // USUWANIE DOMYSLEJ BAZY
       char polecenie[]="DROP DATABASE ";
       strcat(polecenie, pgbase);
       result = PQexec(conn,polecenie);
         cout<<PQresStatus(PQresultStatus(result));
         cout<<"\n";
         cout<<PQresultErrorMessage(result);
         cout<<"\n";
         PQclear(result);
}
/* ************************************************************************ */

string Wczytaj(PGconn *conn, char *adres){

     PGresult *result;

     string napisWlasciwy;
     fstream plik;
     plik.open(adres, ios::in );
     if( plik.good() )
    {
        string napis = "";
        while( !plik.eof() )
        {
            getline( plik, napis );
            napisWlasciwy = napisWlasciwy + napis;
            napisWlasciwy = napisWlasciwy + "\n";
        }
        plik.close();


    } else return "Error! Nie udalo otworzyc sie pliku!\n";


    char wyjscie[napisWlasciwy.length()];
    for (int i =0; i < napisWlasciwy.length(); wyjscie [i++] = '\0\t');
    napisWlasciwy.copy(wyjscie, napisWlasciwy.length());


    result = PQexec(conn, wyjscie);
     cout<<PQresStatus(PQresultStatus(result));
     cout<<"\n";
     cout<<PQresultErrorMessage(result);
     cout<<"\n";
     PQclear(result);


    return wyjscie;


}

/* ************************************************************************ */

void Zaladuj(PGconn *conn, char *sch/*, char *widoki, char *tiggery, char *funkcje, char dane*/)
{
     PGresult *result;
     string pomocnicza1, pomocnicza2;

     Wczytaj(conn, sch);




     Wczytaj(conn, tigerry);

     Wczytaj(conn, widoki);

     Wczytaj(conn, dane);

     Wczytaj(conn,funkcje);



 }


 /* *********************************************************************** */


int main(int argc, char *argv[])
{
PGconn *conn;
PGresult *result;
string sqlCmd = "";

int polecenie;

do{
    cout<<"\n 1) Zaladuj baze\n 2) Zrestartuj baze\n 3) Usun baze\n 4) Zapisz do pliku\n";
    cout<<"------------------\n";
    cout<<" 0) Zakoncz\n\n";
    cin>>polecenie;
    switch(polecenie){

        case 1: /* Utworz Baze */
             conn = PQsetdbLogin(pghost,pgport,NULL,NULL,NULL,pguser,pgpass);
             if (PQstatus(conn) == CONNECTION_OK){

                    UtworzBaze(conn);
                    Rozlacz(conn);
                    conn = PQsetdbLogin(pghost,pgport,NULL,NULL,pgbase,pguser,pgpass);
                     PQsetClientEncoding(conn,"UTF8");

                    if (PQstatus(conn) == CONNECTION_OK){
                                   Zaladuj(conn, szkielet/*, widoki, tiggery, funkcje, dane*/);
                                   Rozlacz(conn);
                                   }else{
                                   cout<<"Error: Blad polaczenia z serverem %s\n";
                                   PQfinish(conn);
                    }

                    }else{
                          cout<<"Error: Blad polaczenia z serverem %s\n";
                          PQfinish(conn);
                          }

              break;


        case 2: /* Restart */

                conn = PQsetdbLogin(pghost,pgport,NULL,NULL,NULL,pguser,pgpass);
                if (PQstatus(conn) == CONNECTION_OK){

                                   Usun(conn);
                                   UtworzBaze(conn);
                                   Rozlacz(conn);

                                   conn = PQsetdbLogin(pghost,pgport,NULL,NULL,pgbase,pguser,pgpass);

                                   if (PQstatus(conn) == CONNECTION_OK){
                                                      Zaladuj(conn, szkielet/*, widoki, tiggery, funkcje, dane*/);
                                                      Rozlacz(conn);
                                   }else{
                                            cout<<"Error: Blad polaczenia z serverem %s\n";
                                            PQfinish(conn);
                                            }

                }else{
                         cout<<"Error: Blad polaczenia z serverem %s\n";
                         PQfinish(conn);
                         }

             break;

        case 3: /* Usun baze */
             conn = PQsetdbLogin(pghost,pgport,NULL,NULL,NULL,pguser,pgpass);
             if (PQstatus(conn) == CONNECTION_OK)
             {
                // Jeoli polaczenie nawizano
                    Usun(conn);
                    Rozlacz(conn);
                }else{
                // Jesli polaczenie nieudane
                    cout<<"Error: Blad polaczenie z serverem %s\n";
                    PQfinish(conn);
                    }
               break;

        case 4:
                char polecenie[255];
                char zmienna[255];
                strcpy(polecenie, "C:/Program_Files/PostgreSQL/9.2/bin/pg_dump.exe");
                strcpy(zmienna, " pg_dump baza > baza.backup");
                strcat(polecenie, zmienna);
                system(polecenie);


            break;
        default :
            cout<<"Podales niewlasciwy numer operacji.!\n Sprobuj ponownie\n\n";
            break;

    }
}while(polecenie != 0);



return 0;
}


