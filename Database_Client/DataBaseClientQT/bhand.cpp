#include "libpq-fe.h"
#include <QFile>
#include <QWidget>
#include <QTableView>
#include <QListView>
#include <QString>
#include <QMessageBox>
#include <QTextStream>
#include <QRegularExpression>
#include "bhand.h"

QString BinderHandler::SQLRefactor(QString in)
{
    QRegularExpression RE;
    RE.setPattern("--[A-Za-z0-9\ !;)]*\n");
    in.remove(RE);

    in.replace("\n", " ");
    in.replace(";", "\n");
    in.replace("\t", "");
    in.replace("\r", "");
    in.replace("\012", "");
    in.replace("\015", "");
    return in;
}

QString BinderHandler::SQLOpenFile(const char *path)
{
    QFile file(path);
    if (file.open(QIODevice::ReadOnly | QIODevice::Text))
    {
        QTextStream in(&file);
        return in.readAll();
    }
    else
    {
        ShowInfoBox("Nie można otworzyć " + file.fileName());
    }
    return NULL;
}

void BinderHandler::DBConnect()
{
    DBConnPointer = PQsetdbLogin(this->pghost,this->pgport,
                                 this->pgoptions,this->pgtty,
                                 this->dbName,this->login,this->pwd);
}

void BinderHandler::DBDisconnect()
{
    PQclear(DBResultPointer);
    PQfinish(DBConnPointer);
}

bool BinderHandler::isConnectionOk()
{
    return PQstatus(this->DBConnPointer) == CONNECTION_OK;
}

void BinderHandler::ShowErrWithConnection()
{
    QString errmsg;
    errmsg += "Error: Blad polaczenia z serverem.\n";
    errmsg += PQerrorMessage(DBConnPointer);

    QMessageBox msgBox;
    msgBox.setText(errmsg);
    msgBox.setIcon(QMessageBox::Critical);
    msgBox.exec();
}

void BinderHandler::ShowErrCmdStatusBox()
{
    QString msg;
    msg += PQresStatus(PQresultStatus(DBResultPointer));
    msg += ":\n";
    msg += PQresultErrorMessage(DBResultPointer);

    QMessageBox msgBox;
    msgBox.setText(msg);
    msgBox.setIcon(QMessageBox::Critical);
    msgBox.exec();
}

BinderHandler::BinderHandler()
{
    pghost = NULL;
    pgport = NULL;
    pgoptions = NULL;
    pgtty = NULL;
    dbName = NULL;
    login = NULL;
    pwd = NULL;
}

BinderHandler::~BinderHandler()
{
    PQclear(DBResultPointer);
}

const char* QStrToCStr(QString qstring)
{
    return qstring.toStdString().c_str();
}

void ShowInfoBox(QString string)
{
    QMessageBox msgBox;
    msgBox.setIcon(QMessageBox::Information);
    msgBox.setText(string);
    msgBox.exec();
}

QString IToQS(int x)
{
    return QString::number(x);
}

QString DToQS(double x)
{
    return QString::number(x);
}

void BinderHandler::SetDBConnStrings(const char *pghost, const char *pgport,
                                const char *pgoptions, const char *pgtty,
                                const char *dbName, const char *login,
                                const char *pwd)
{
    this->pghost = pghost;
    this->pgport = pgport;
    this->pgoptions = pgoptions;
    this->pgtty = pgtty;
    this->dbName = dbName;
    this->login = login;
    this->pwd = pwd;
}

