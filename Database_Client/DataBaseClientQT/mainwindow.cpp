#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "bhand.h"
#include <QFile>
#include <QTextStream>
#include <QRegularExpression>

const char pghost[] = "localhost";
const char pgport[] = "5432";
const char pguser[] = "postgres";
const char pgpass[] = "postgres";
const char pgdbname[] = "baza1";

char szkielet[] = "../sql/szkielet_bazy.sql";
char widoki[] = "../sql/widoki.sql";
char triggery[] = "../sql/wyzwalacze.sql";
char funkcje[] = "../sql/funkcje.sql";
char dane[] = "../sql/dane_poczatkowe.sql";

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    BHandler.SetDBConnStrings(pghost, pgport, NULL, NULL, pgdbname, pguser, pgpass);
    ui->setupUi(this);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_pushButton_clicked()
{
    BHandler.SetDBConnStrings(pghost, pgport, NULL, NULL, NULL, pguser, pgpass);
    BHandler.DBConnect();

    if (!BHandler.isConnectionOk())
    {
        BHandler.ShowErrWithConnection();
    }else
    {
        QString strCmd="";

        strCmd += "CREATE DATABASE ";
        strCmd += pgdbname;

        BHandler.DBResultPointer = PQexec(BHandler.DBConnPointer, QStrToCStr(strCmd));

        if ( PQresultStatus(BHandler.DBResultPointer) != PGRES_COMMAND_OK)
            BHandler.ShowErrCmdStatusBox();
        else{

            BHandler.DBDisconnect();
            BHandler.SetDBConnStrings(pghost, pgport, NULL, NULL, pgdbname, pguser, pgpass);
            BHandler.DBConnect();

            strCmd = "";

            const char* pliki[] =
            {
                szkielet,
                triggery,
                widoki,
                funkcje,
                dane
            };

            for (int i = 0; i<5; i++)
            {
                strCmd = BHandler.SQLOpenFile(pliki[i]);
                if (strCmd != NULL)
                {
                    //strCmd = BHandler.SQLRefactor(strCmd);

                    BHandler.DBResultPointer = PQexec(BHandler.DBConnPointer, strCmd.toLocal8Bit().data());

                    if ( PQresultStatus(BHandler.DBResultPointer) != PGRES_COMMAND_OK)
                        BHandler.ShowErrCmdStatusBox();
                    else if (i == 4)
                    {
                        QString info = "Pomyślnie załadowano bazę ";
                        info += pgdbname;
                        info += "!";
                        ShowInfoBox(info);
                    }
                }
            }


            ui->plainTextEdit->setPlainText(strCmd);
        }
    }
    BHandler.DBDisconnect();
}

void MainWindow::on_pushButton_2_clicked()
{
    BHandler.DBConnect();

    if (!BHandler.isConnectionOk())
    {
        BHandler.ShowErrWithConnection();
    }else
    {

        QString strCmd;

        strCmd = "SELECT pg_terminate_backend(pg_stat_activity.pid) ";
        strCmd += "FROM pg_stat_activity WHERE pg_stat_activity.datname = '";
        strCmd += pgdbname;
        strCmd += "';";

        BHandler.DBResultPointer = PQexec(BHandler.DBConnPointer, QStrToCStr(strCmd));

        if ( PQresultStatus(BHandler.DBResultPointer) != PGRES_TUPLES_OK)
            BHandler.ShowErrCmdStatusBox();

        //BHandler.DBDisconnect();
        //BHandler.DBConnect();
        if (!BHandler.isConnectionOk())
        {
            BHandler.ShowErrWithConnection();
        }else
        {

            strCmd = "DROP DATABASE ";
            strCmd += pgdbname;

            BHandler.DBResultPointer = PQexec(BHandler.DBConnPointer, QStrToCStr(strCmd));
            if ( PQresultStatus(BHandler.DBResultPointer) != PGRES_COMMAND_OK)
                BHandler.ShowErrCmdStatusBox();
            else
            {
                QString info = "";
                info += "Usunięto pomyślnie bazę :";
                info += pgdbname;
                info += " !";
                ShowInfoBox(info);
            }
        }
    }
    BHandler.DBDisconnect();
}
