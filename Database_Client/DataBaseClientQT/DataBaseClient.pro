#-------------------------------------------------
#
# Project created by QtCreator 2013-06-14T19:59:19
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets


INCLUDEPATH += C:/pqsql/include
LIBS += C:/pqsql/lib/libpq.lib

TARGET = DataBaseClient
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    bhand.cpp

HEADERS  += mainwindow.h \
    bhand.h

FORMS    += mainwindow.ui
