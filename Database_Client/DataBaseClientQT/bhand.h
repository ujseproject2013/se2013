#ifndef BINDERHADLER
#define BINDERHADLER


#include "libpq-fe.h"
#include <QString>
#include <QWidget>
#include <QTableView>
#include <QListView>
#include <QString>
#include <QMessageBox>


const char* QStrToCStr(QString qstring);
void ShowInfoBox(QString string);
QString IToQS(int x);
QString DToQS(double x);

class BinderHandler
{

public :

    PGconn* DBConnPointer;
    PGresult* DBResultPointer;

    const char *pghost;
    const char *pgport;
    const char *pgoptions;
    const char *pgtty;
    const char *dbName;
    const char *login;
    const char *pwd;

    void DBConnect();
    void DBDisconnect();
    bool isConnectionOk();
    void ShowErrWithConnection();
    void ShowErrCmdStatusBox();
    void updateRow(QString tableName, QString columnName, QString newValue, QString ID);
    QString SQLRefactor(QString in);
    QString SQLOpenFile(const char* path);

    //---------------------------------------------

    //-------Konstruktory--------------------------

    BinderHandler();
    ~BinderHandler();

    //---------------------------------------------

    //---------------------------------------------

    void SetDBConnStrings(const char *pghost, const char *pgport,
                          const char *pgoptions, const char *pgtty,
                          const char *dbName, const char *login,
                          const char *pwd);

};
#endif
