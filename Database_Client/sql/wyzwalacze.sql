
CREATE OR REPLACE FUNCTION max_Min()
RETURNS TRIGGER AS $a$
DECLARE
i int;
min money;
max money;
BEGIN
		i = NEW.kadry_id;
		min = (SELECT min_zarobki from SCHEMAT.kadry WHERE id = i);
		max = (SELECT max_zarobki from SCHEMAT.kadry WHERE id = i);
		IF NEW.pensja > max THEN 
			NEW.pensja = (SELECT max_zarobki from SCHEMAT.kadry WHERE id = i);
		END IF;
		IF NEW.pensja < min THEN
			NEW.pensja = min;
		END IF;
		IF  (NEW.pensja IS NULL) THEN 
			NEW.pensja = min;
		END IF; --zmiana pensji
		IF (TG_OP = 'INSERT') THEN
			update SCHEMAT.kadry SET il_osob_w_kadrze = (il_osob_w_kadrze +1) WHERE id = NEW.kadry_id;
		END IF;
		IF (TG_OP = 'UPDATE' AND (OLD.kadry_id <> NEW.kadry_id)) THEN
			update SCHEMAT.kadry SET il_osob_w_kadrze = (il_osob_w_kadrze+1) WHERE id = NEW.kadry_id;
			update SCHEMAT.kadry SET il_osob_w_kadrze = (il_osob_w_kadrze-1) WHERE id = OLD.kadry_id;
		END IF;
                RETURN NEW;
		
END
$a$
LANGUAGE plpgsql;

CREATE TRIGGER kadry_max_min
BEFORE INSERT OR UPDATE ON SCHEMAT.pracownicy
FOR EACH ROW EXECUTE PROCEDURE max_Min();


/*
TRIGGER control column pensja in Pracownik table;
*/

CREATE OR REPLACE FUNCTION zlecenie_wewnetrzne()
RETURNS TRIGGER AS $b$
DECLARE 
a varchar(255);
b int;
c varchar(4);
BEGIN
	c = NEW.ID::varchar(4);
	a = 'kuchnia' || c;
	b = (SELECT COUNT(*) from SCHEMAT.klient)+1;
	insert into SCHEMAT.klient values (b,'zle_wewnetrzne',a,NEW.adresid);
	RETURN NEW;
END
$b$
LANGUAGE PLPGSQL;

CREATE TRIGGER zlecenie
BEFORE INSERT ON SCHEMAT.kuchnia
FOR EACH ROW EXECUTE PROCEDURE zlecenie_wewnetrzne();

/*
IF YOU ADD NEW KUCHNIA TRIGGER CREATE NEW KLIENT ZLECENIE WEWNETRZE, EVERY KITCHEN IS A CLIENT!

*/


CREATE OR REPLACE FUNCTION przepisy() 
RETURNS TRIGGER AS $ccc$
DECLARE 
a varchar(255);
b int;
BEGIN
		IF NOT EXISTS ( SELECT * 
					from SCHEMAT.nazwy_skladnikow
					WHERE nazwa = NEW.Nazwa_Towaru
					) THEN
			IF (NEW.spozywczy = true) THEN
				EXECUTE 'Alter table SCHEMAT.Przepis add  ' || NEW.Nazwa_Towaru || ' FLOAT';
				-- PURE AWESOME !!!!!!!!!!!!!!!!!!!!
				insert into SCHEMAT.nazwy_skladnikow values ((SELECT COUNT(*) from SCHEMAT.nazwy_skladnikow)+1,
						NEW.Nazwa_Towaru);
			END IF;
		END IF;
		RETURN NEW;	
END
$ccc$
LANGUAGE PLPGSQL;

/*
IF YOU ADD NEW 'TOWAR' TABLE PRZEPIS HAVE NEW COLUMN!!!
*/

CREATE TRIGGER przepis
BEFORE INSERT ON SCHEMAT.Towar
FOR EACH ROW EXECUTE PROCEDURE przepisy();

/*
TO DO : TRIGGER DO ZAMOWIEN
*/

CREATE OR REPLACE FUNCTION history()
RETURNS TRIGGER AS $d$
DECLARE 
	LEN int;
	iimie varchar(100);
	inazwisko varchar(255);
	iulica varchar(120);
	iNr_mieszkania int;
	iNr_Budynku int;
	iMiasto varchar(120);
	iKurier_ID int;
	iCena money;
	inr_telefonu int;
	iemail varchar(255);
BEGIN
	IF (TG_OP = 'INSERT' OR TG_OP = 'UPDATE') THEN
		IF (NEW.Stan_Zamowienia = 4 OR NEW.Stan_Zamowienia = 3 ) THEN
			LEN = (SELECT count(*) from SCHEMAT.OLD_zamowienia);
			iimie = (SELECT imie from SCHEMAT.Klient WHERE id = NEW.klientid);
			inazwisko = (SELECT nazwisko from SCHEMAT.Klient WHERE id = NEW.klientid);
			iulica = (SELECT Ulica from SCHEMAT.adres WHERE id = (SELECT AdresId from SCHEMAT.klient WHERE id = NEW.klientid));
			iNr_mieszkania = (SELECT Nr_Mieszkania from SCHEMAT.adres WHERE id = (SELECT AdresId from SCHEMAT.klient WHERE id = NEW.klientid));
			iNr_Budynku = (SELECT Nr_Budynku from SCHEMAT.adres WHERE id = (SELECT AdresId from SCHEMAT.klient WHERE id = NEW.klientid));
			iMiasto = (SELECT Miasto from SCHEMAT.adres WHERE id = (SELECT AdresId from SCHEMAT.klient WHERE id = NEW.klientid));
			inr_telefonu = (SELECT nr_telefonu from SCHEMAT.adres WHERE id = (SELECT AdresId from SCHEMAT.klient WHERE id = NEW.klientid));
			iemail = (SELECT email from SCHEMAT.adres WHERE id = (SELECT AdresId from SCHEMAT.klient WHERE id = NEW.klientid));
			IF (NEW.Stan_Zamowienia = 3) THEN 
				iKurier_ID = 0;
			END IF;
			IF (NEW.Stan_Zamowienia = 4) THEN
				iKurier_ID = (SELECT kurierid from SCHEMAT.dostawa WHERE zamowienieID = NEW.ID);
			END IF;
			iCena = NEW.Cena;
			IF (NEW.Stan_Zamowienia = 3) THEN
				INSERT INTO SCHEMAT.OLD_zamowienia values (LEN+1,NEW.KuchniaID,NEW.ProduktID,iimie,inazwisko,iulica,
						iNr_mieszkania,iNr_budynku,iMiasto,iKurier_ID,iCena,inr_telefonu,iemail,false);
			ELSIF (NEW.Stan_Zamowienia = 4) THEN
				INSERT INTO SCHEMAT.OLD_zamowienia values (LEN+1,NEW.KuchniaID,NEW.ProduktID,iimie,inazwisko,iulica,
						iNr_mieszkania,iNr_budynku,iMiasto,iKurier_ID,iCena,inr_telefonu,iemail,true);
			END IF;
		END IF;	
		RETURN NEW;
	ELSIF (TG_OP = 'DELETE') THEN
		IF (OLD.Stan_zamowienia <> 4 AND OLD.Stan_zamowienia <> 3) THEN
			LEN = (SELECT count(*) from SCHEMAT.OLD_zamowienia);
			iimie = (SELECT imie from SCHEMAT.Klient WHERE id = OLD.klientid);
			inazwisko = (SELECT nazwisko from SCHEMAT.Klient WHERE id = OLD.klientid);
			iulica = (SELECT Ulica from SCHEMAT.adres WHERE id = (SELECT AdresId from SCHEMAT.klient WHERE id = OLD.klientid));
			iNr_mieszkania = (SELECT Nr_Mieszkania from SCHEMAT.adres WHERE id = (SELECT AdresId from SCHEMAT.klient WHERE id = OLD.klientid));
			iNr_Budynku = (SELECT Nr_Budynku from SCHEMAT.adres WHERE id = (SELECT AdresId from SCHEMAT.klient WHERE id = OLD.klientid));
			iMiasto = (SELECT Miasto from SCHEMAT.adres WHERE id = (SELECT AdresId from SCHEMAT.klient WHERE id = OLD.klientid));
			inr_telefonu = (SELECT nr_telefonu from SCHEMAT.adres WHERE id = (SELECT AdresId from SCHEMAT.klient WHERE id =OLD.klientid));
			iemail = (SELECT email from SCHEMAT.adres WHERE id = (SELECT AdresId from SCHEMAT.klient WHERE id = OLD.klientid));
			IF (OLD.Stan_Zamowienia = 3) THEN 
				iKurier_ID = 0;
			END IF;
			IF (OLD.Stan_Zamowienia = 4) THEN
				iKurier_ID = (SELECT kurierid from SCHEMAT.dostawa WHERE zamowienieID = OLD.ID);
			END IF;
			iCena = OLD.Cena;
			INSERT INTO SCHEMAT.OLD_zamowienia values (LEN+1,OLD.KuchniaID,OLD.ProduktID,iimie,inazwisko,iulica,
						iNr_mieszkania,iNr_budynku,iMiasto,iKurier_ID,iCena,inr_telefonu,iemail,false);
		END IF;
		RETURN OLD;
	END IF;
END
$d$
LANGUAGE PLPGSQL;

CREATE TRIGGER historia
BEFORE UPDATE OR INSERT OR DELETE ON SCHEMAT.Zamowienie
FOR EACH ROW EXECUTE PROCEDURE history();

/*
TRIGGER to keep history in database!

*/

CREATE OR REPLACE FUNCTION doliczaj_kucharzy()
RETURNS TRIGGER AS $C$
DECLARE
zdrowy bool;
BEGIN
	IF (TG_OP = 'INSERT') THEN 
		update SCHEMAT.kuchnia SET ilosc_kucharzy  = (ilosc_kucharzy+1) WHERE id = NEW.kuchniaid;
		zdrowy = (SELECT urlop from SCHEMAT.pracownicy WHERE id = NEW.pracownicyid);
		IF (zdrowy IS NOT TRUE ) THEN
			update SCHEMAT.kuchnia SET ilosc_aktywnych_kucharzy = (ilosc_aktywnych_kucharzy+1) WHERE id = NEW.kuchniaid;
		END IF;
		RETURN NEW;
	ELSIF (TG_OP = 'DELETE') THEN
		update SCHEMAT.kuchnia SET ilosc_kucharzy  = (ilosc_kucharzy-1) WHERE id = OLD.kuchniaid;
		zdrowy = (SELECT urlop from SCHEMAT.pracownicy WHERE id = OLD.pracownicyid);
		IF (zdrowy IS NOT TRUE) THEN
			update SCHEMAT.kuchnia SET ilosc_aktywnych_kucharzy = (ilosc_aktywnych_kucharzy-1) WHERE id = OLD.kuchniaid;
		END IF;
		RETURN OLD;
	END IF;
END
$C$
LANGUAGE PLPGSQL;

CREATE TRIGGER kucharze
BEFORE INSERT OR DELETE ON SCHEMAT.kucharz
FOR EACH ROW EXECUTE PROCEDURE doliczaj_kucharzy(); 


CREATE OR REPLACE FUNCTION urlop_kucharzy()
RETURNS TRIGGER AS $D$
BEGIN
	IF (TG_OP = 'UPDATE') THEN
		IF (NEW.kadry_id = 2 AND NEW.urlop IS TRUE) THEN
			update SCHEMAT.kuchnia SET ilosc_aktywnych_kucharzy = (ilosc_aktywnych_kucharzy-1) 
				WHERE id = (SELECT kuchniaid from SCHEMAT.kucharz WHERE pracownicyid = NEW.ID);
		ELSIF (NEW.kadry_id = 2 AND OLD.urlop IS TRUE AND (NEW.urlop IS NOT TRUE)) THEN
			update SCHEMAT.kuchnia SET ilosc_aktywnych_kucharzy = (ilosc_aktywnych_kucharzy+1)
				WHERE id = (SELECT kuchniaid from SCHEMAT.kucharz WHERE pracownicyid = NEW.ID);
		END IF;
		RETURN NEW;
	END IF;
END
$D$
LANGUAGE PLPGSQL;

CREATE TRIGGER urlopy_kuchni
BEFORE UPDATE ON SCHEMAT.pracownicy
FOR EACH ROW EXECUTE PROCEDURE urlop_kucharzy();


/*
TRIGGERs to update status ilosc_aktywnych_pracownikow and ilosc_Pracownikow in table kuchnia
*/
CREATE OR REPLACE FUNCTION usun_adres()
RETURNS TRIGGER AS $$
DECLARE 
a int;
BEGIN
        DELETE FROM SCHEMAT.adres WHERE id = OLD.adres_id;
        a = (SELECT il_osob_w_kadrze from SCHEMAT.kadry WHERE id = OLD.kadry_id);
        a = a -1;
        UPDATE SCHEMAT.kadry SET il_osob_w_kadrze = a WHERE id = OLD.kadry_id;

        RETURN OLD;
END
$$
LANGUAGE PLPGSQL;


CREATE TRIGGER del_adres
AFTER DELETE ON SCHEMAT.pracownicy
FOR EACH ROW EXECUTE PROCEDURE usun_adres();

--TRIGGER USUWAJACY PRZY USUNIECIU PRACOWNIKA JEGO ADRES!

CREATE OR REPLACE FUNCTION add_nieprzydzielona_dostawa()
RETURNS TRIGGER AS $$
DECLARE
a int;
BEGIN
	-- HOUSTON WE HAVE A PROBLEM
	a = (SELECT MAX(id) FROM SCHEMAT.nieprzydzielona_dostawa) + 1;
	
	INSERT INTO SCHEMAT.Nieprzydzielona_dostawa 
		( Select a, d.KlientID,d.ZamowienieID,
			d.MagazynID 
			FROM schemat.dostawa d
		 );
--
	RETURN OLD;
END
$$
LANGUAGE PLPGSQL;

CREATE TRIGGER usun_kuriera
BEFORE DELETE ON schemat.Kurier
FOR EACH ROW EXECUTE PROCEDURE add_nieprzydzielona_dostawa();

CREATE OR REPLACE FUNCTION update_stan()
RETURNS TRIGGER AS $$
DECLARE
a int;
BEGIN
	a = (SELECT p.ile FROM schemat.produkt p
			WHERE p.id = NEW.produktid
		);
	IF (NEW.stan_zamowienia = 2 OR NEW.stan_zamowienia = 4) THEN
		UPDATE schemat.produkt set ile = a - 1
		WHERE id = NEW.produktid;
	ELSIF (NEW.stan_zamowienia = 3) THEN
		UPDATE schemat.produkt set ile = a - 1
		WHERE id = NEW.produktid;
	END IF;

	RETURN NEW;
END
$$
LANGUAGE PLPGSQL;

CREATE TRIGGER zmien_stan
AFTER UPDATE ON schemat.Zamowienie
FOR EACH ROW EXECUTE PROCEDURE update_stan();
