
CREATE SCHEMA schemat;

CREATE TABLE schemat.Kadry (
	Id SERIAL PRIMARY KEY,
	Nazwa_Kadry varchar (45) NOT NULL,
	Min_zarobki money NOT NULL,
	Max_zarobki money NOT NULL,
	Il_osob_w_kadrze  int DEFAULT 0
);

CREATE TABLE schemat.Adres (
	Id SERIAL PRIMARY KEY UNIQUE,
	Ulica varchar(120) NOT NULL,
	Nr_Mieszkania int,
	Nr_Budynku int,
	Miasto varchar(120) NOT NULL,
	Kod_pocztowy int NOT NULL,
	nr_telefonu int NOT NULL,
	email varchar(255) NOT NULL UNIQUE
);

CREATE TABLE schemat.Pracownicy (
	Id SERIAL PRIMARY KEY,
	Imie varchar(45) NOT NULL,
	Nazwisko varchar (120) NOT NULL,
	Pensja money,
	Stanowisko int NOT NULL,
	Adres_ID int REFERENCES schemat.Adres(Id) ON DELETE CASCADE,
	Kadry_ID int REFERENCES schemat.Kadry(Id) ON DELETE CASCADE,
	Urlop bool,
	Koniec_Urlopu date
);

CREATE TABLE schemat.Kurier (
	Id SERIAL PRIMARY KEY,
	Nr_Pagera int NOT NULL,
	Pojemnosc int NOT NULL,
	Przejechane_Godziny int,
	Pracownicy_ID int REFERENCES schemat.Pracownicy(Id) ON DELETE CASCADE
);

CREATE TABLE schemat.Przepis (
	Id SERIAL PRIMARY KEY,	
	Nazwa_Przepisu varchar(120) NOT NULL
	-- To na samym koncu trzeba robic kopiowanie TOWAROW!!!
	-- Bo nie ma tabeli towary i nie mozna jej wczesniej stworzyc ;p
	-- bedzie dodawane triggerem !
);


CREATE TABLE schemat.Magazyn (
	Id SERIAL PRIMARY KEY,
	Nazwa_Magazynu varchar(100) NOT NULL,
	Pojemnosc_Magazynu int,
	AdresId int REFERENCES schemat.Adres(Id)
);

CREATE TABLE schemat.Produkt (
	Id SERIAL PRIMARY KEY,
	Nazwa_Produktu varchar(120) NOT NULL,
	Cena_Produktu money NOT NULL,
	Koszt_Produktu money NOT NULL,
	ile int DEFAULT 1,
	Przepis_ID int REFERENCES schemat.Przepis(Id),
	Magazyn_ID int REFERENCES schemat.Magazyn(Id),
	Dostepny boolean DEFAULT TRUE
);

CREATE TABLE schemat.Kuchnia (
	Id SERIAL PRIMARY KEY,
	Ilosc_Kucharzy int NOT NULL,
	Ilosc_Aktywnych_Kucharzy int NOT NULL,
	Zdolna_do_pracy bool NOT NULL,
	AdresID int REFERENCES schemat.Adres(Id) ,
	MagazynID int REFERENCES schemat.Magazyn(Id)
);

CREATE TABLE schemat.Kucharz(
	Id SERIAL PRIMARY KEY,
	Specjalnosc varchar(200) NOT NULL,
	Stanowisko int NOT NULL,
	Doswiadczenie int,
	PracownicyID int REFERENCES schemat.Pracownicy(Id) ON DELETE CASCADE,
	KuchniaID int REFERENCES schemat.Kuchnia(Id)
); 

CREATE TABLE schemat.Klient (
	Id SERIAL PRIMARY KEY,
	Imie varchar(100) NOT NULL,
	Nazwisko varchar(255) NOT NULL,
	AdresID int REFERENCES schemat.Adres(Id)
);

CREATE TABLE schemat.Zamowienie (
	Id SERIAL PRIMARY KEY,
	KuchniaID int REFERENCES schemat.Kuchnia(Id),
	KlientID int REFERENCES schemat.Klient(Id),
	ProduktId int REFERENCES schemat.Produkt(Id),
	Cena money NOT NULL,
	Stan_Zamowienia int NOT NULL,
	Komentarz varchar(300)
);

CREATE TABLE schemat.Produkcja (
	Id SERIAL PRIMARY KEY,
	KuchniaID int REFERENCES schemat.Kuchnia(Id),
	ProduktID int REFERENCES schemat.Produkt(Id),
	ZamowienieID int REFERENCES schemat.Zamowienie(Id) ON DELETE CASCADE,
	MagazynID int REFERENCES schemat.Magazyn(Id)
);

CREATE TABLE schemat.Dostawa (
	Id SERIAL PRIMARY KEY,
	KurierID int REFERENCES schemat.Kurier(Id) ON DELETE CASCADE,
	KlientID int REFERENCES schemat.Klient(Id) ON DELETE CASCADE,
	ZamowienieID int REFERENCES schemat.Zamowienie(Id) ON DELETE CASCADE,
	MagazynID int REFERENCES schemat.Magazyn(Id) ON DELETE CASCADE
);

CREATE TABLE schemat.Nieprzydzielona_dostawa (
	ID SERIAL PRIMARY KEy,
	KlientID int REFERENCES schemat.Klient(Id) ON DELETE CASCADE,
	ZamowienieID int REFERENCES schemat.Zamowienie(Id) ON DELETE CASCADE,
	MagazynID int REFERENCES schemat.Magazyn(Id) ON DELETE CASCADE
);

CREATE TABLE schemat.Towar (
	Id SERIAL PRIMARY KEY,
	Nazwa_Towaru varchar(255) NOT NULL,
	Cena_Towaru money NOT NULL,
	Ilosc int NOT NULL,
	MagazynID int REFERENCES schemat.Magazyn(Id),
	KuchniaID int REFERENCES schemat.Kuchnia(Id),
	spozywczy bool NOT NULL
);

CREATE TABLE schemat.nazwy_skladnikow (
	ID SERIAL PRIMARY KEY,
	nazwa varchar(255)
);

CREATE TABLE schemat.old_zamowienia (
	ID SERIAL PRIMARY KEY,
	Kuchnia_ID int NOT NULL,
	Produkt_ID int NOT NULL,
	imie varchar(100),
	nazwisko varchar(255),
	ulica varchar(120),
	Nr_mieszkania int,
	Nr_Budynku int,
	Miasto varchar(120),
	Kurier_ID int, 
	Cena money,
	nr_telefonu int,
	email varchar(255),
	Wykonane bool NOT NULL
);
-----
