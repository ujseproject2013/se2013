insert into schemat.kadry values (1,'kurier',1200,1400);
insert into schemat.kadry values (2,'kucharz',1400,1800);
insert into schemat.kadry values (3,'szef',2000,3200);

insert into schemat.adres values (1,'Piastowska',1,1,'Krakow',3232,312312,'ex1amp2le@ex.pl');
insert into schemat.adres values (2,'Lojasiewicza',1,0,'Krakow',3226032,43422,'ex2amp1le@ex.pl');
insert into schemat.adres values (3,'Aleje',7,6,'Krakow',3226032,23423,'exa3mp2le@ex.pl');
insert into schemat.adres values (4,'Aleje',5,7,'Krakow',3226032,323423,'exam432ple@ex.pl');
insert into schemat.adres values (5,'Mickiewicza',3,6,'Krakow',3226032,24342,'exa5m32ple@ex.pl');
insert into schemat.adres values (6,'Poniatowskiego',5,9,'Krakow',3226032,654654,'exa632mple@ex.pl');
insert into schemat.adres values (7,'Wiliczenko',11,62,'Krakow',3226032,3456745,'exam832ple@ex.pl');
insert into schemat.adres values (8,'Zwyciezcow',5,63,'Krakow',322032,45754,'ex23am9ple@ex.pl');
insert into schemat.adres values (9,'Sukcesu',2,7,'Krakow',322,603245645,'exam10ple32@ex.pl');
insert into schemat.adres values (10,'Porazki',5,12,'Krakow',3226032,78964,'ex11ample12@ex.pl');
insert into schemat.adres values (11,'Powstancow',32,6,'Krakow',3226032,67695,'e12xample22@ex.pl');
insert into schemat.adres values (12,'Sadysty',19,7,'Krakow',3226032,7897789,'exa13mple3@ex.pl');
insert into schemat.adres values (13,'Matek',5,0,'Krakow',3226032,7978987,'exam14ple1@ex.pl');
insert into schemat.adres values (14,'Szpitalna',10,0,'Krakow',3226032,978987,'ew15eq@ex.pl');
insert into schemat.adres values (15,'Miesna',5,23,'Krakow',3226032,78978978,'exam16ple2@ex.pl');
insert into schemat.adres values (16,'Wolowa',5,34,'Krakow',3226032,78978978,'exam17ple3@ex.pl');
insert into schemat.adres values (17,'Owca',5,6,'Krakow',3226032,79789789,'exampl18e4@ex.pl');
insert into schemat.adres values (18,'Sklepowa',5,6,'Krakow',1111111,1111111,'exam19ple4@ex.pl');
insert into schemat.adres values (19,'Kliencka1',3,0,'Krakow',232132,3232323,'exam20ple#@cos.pl');
insert into schemat.adres values (20,'Kliencka2',10,10,'Krakow',440404,404040,'exam21ple#@cos.pl');
insert into schemat.adres values (21,'Kliencka4',12,3,'Cracow',33333,33333,'exam22ple#@cos.pl');
insert into schemat.adres values (22,'Kliencka4',5,0,'Warsaw',22222,22222,'examp23le#@cos.pl');

insert into schemat.pracownicy values (1,'Ania', 'Nowak',1200,1,1,1,false);
insert into schemat.pracownicy values (2,'Michal', 'Nowak',1200,1,2,1,false);
insert into schemat.pracownicy values (3,'Artur', 'Kowalski',1300,1,3,2,false);
insert into schemat.pracownicy values (4,'Stanislaw','Kowalski',1400,2,4,2,false);
insert into schemat.pracownicy values (5,'Marek','Koza',2000,1,5,3,false);
insert into schemat.pracownicy values (6,'Igor','Messi',1200,2,6,2,false);
insert into schemat.pracownicy values (7,'Piotr','Nowak',1300,2,7,2,false);
insert into schemat.pracownicy values (8,'Marian','Kot',1400,2,8,2,false);
insert into schemat.pracownicy values (9,'Adam','Pies',1500,2,9,2,false);
insert into schemat.pracownicy values (10,'Marek', 'Nowak',1600,2,10,2,false);
insert into schemat.pracownicy values (11,'Artur','Pieklo',1200,2,11,2,false);
insert into schemat.pracownicy values (12,'Maciek','Traktor',2500,0,12,3,false);
insert into schemat.pracownicy values (13,'Igor','Ogor',1200,2,13,1,false);
insert into schemat.pracownicy values (14,'Artur','Adamek',1400,2,14,1,false);
insert into schemat.pracownicy values (15,'Jan','Mors',1500,1,15,1,true,'2013-08-10');
insert into schemat.pracownicy values (16,'Lol','Owned',1900,1,16,1,false);
insert into schemat.pracownicy values (17,'Pwned','xD',2000,0,17,3,false);

insert into schemat.Kurier values (1,1,2000,0,13);
insert into schemat.Kurier values (2,2,2000,0,14);
insert into schemat.Kurier values (3,3,2000,0,15);
insert into schemat.Kurier values (4,4,2000,0,16);

insert into schemat.Przepis values (1,'pierogi babci ireny');
insert into schemat.Przepis values (2,'golabki mamy');
insert into schemat.Przepis values (3,'zurek');

insert into schemat.Magazyn values (1,'GLOWNY',10000,18);

insert into schemat.Produkt values (1,'pierogi',18,2,10,1,1);
insert into schemat.Produkt values (2,'spaghetti',15,1,6,2,1);
insert into schemat.Produkt values (3,'zurek',12,3,6,3,1);

insert into schemat.Kuchnia values (1,0,0,true,18,1);

insert into schemat.Kucharz values (1,'pierogi',0,1,1,1);
insert into schemat.Kucharz values (2,'pierogi',0,1,2,1);
insert into schemat.Kucharz values (3,'pierogi',0,1,3,1);
insert into schemat.Kucharz values (4,'spaghetti',0,2,4,1);
insert into schemat.Kucharz values (5,'spaghetti',0,2,6,1);
insert into schemat.Kucharz values (6,'spaghetti',0,2,7,1);
insert into schemat.Kucharz values (7,'spaghetti',0,2,8,1);
insert into schemat.Kucharz values (8,'zurek',0,1,9,1);
insert into schemat.Kucharz values (9,'zurek',0,1,10,1);
insert into schemat.Kucharz values (10,'zurek',0,1,11,1);

insert into schemat.Klient values (2,'Marcin','Maj',19);
insert into schemat.Klient values (3,'Adam','XXX',20);
insert into schemat.Klient values (4,'Kamil','YYY',21);
insert into schemat.Klient values (5,'Marek','ZZZ',22);

insert into schemat.Zamowienie values (1,1,2,2,10,0);
insert into schemat.Zamowienie values (2,1,2,2,10,1);
insert into schemat.Zamowienie values (3,1,2,3,10,1);
insert into schemat.Zamowienie values (4,1,3,2,10,1);
insert into schemat.Zamowienie values (5,1,4,2,10,1);
insert into schemat.Zamowienie values (6,1,4,2,10,2);

insert into schemat.Produkcja values (1,1,1,1,1);
insert into schemat.Produkcja values (2,1,1,2,1);
insert into schemat.Produkcja values (3,1,2,4,1);
insert into schemat.Produkcja values (4,1,2,5,1);
insert into schemat.Produkcja values (5,1,2,6,1);
insert into schemat.Produkcja values (6,1,3,3,1);

insert into schemat.Dostawa values (1,1,4,6,1);

insert into schemat.Towar values (1,'Jajka',60,200,1,1,true);
insert into schemat.Towar values (2,'Mleko',30,150,1,1,true);
insert into schemat.Towar values (3,'Maka_ziemniaczana',25,100,1,1,true);
insert into schemat.Towar values (4,'Maka_pszenna',30,300,1,1,true);
insert into schemat.Towar values (5,'Mieso',20,100,1,1,true);
insert into schemat.Towar values (6,'Kapusta',60,120,1,1,true);
insert into schemat.Towar values (7,'Olej',40,90,1,1,true);
insert into schemat.Towar values (8,'Maslo',10,30,1,1,true);
insert into schemat.Towar values (9,'Przyprawy',10,20,1,1,true);
insert into schemat.Towar values (10,'Mydlo',5,6,1,1,false);

update schemat.przepis set jajka = 3,mleko = 2, maka_pszenna = 0.5, mieso = 0.5,kapusta = 1, olej = 0.2,maslo = 0.5 where id = 1;
update schemat.przepis set jajka = 2,Maka_ziemniaczana = 1,mieso = 0.5,kapusta = 1,olej = 0.3,maslo = 0.5 where id = 2;
update schemat.przepis set jajka = 1,mieso = 2,Przyprawy = 2;

----
