﻿CREATE OR REPLACE VIEW pokaz_klientow AS 
	select k.imie AS "Imie", k.nazwisko AS "Nazwisko", a.Miasto AS "Miasto zamieszkania", a.kod_pocztowy AS "Kod pocztowy",a.
			Ulica AS "Ulica", a.nr_mieszkania AS "Nr. Mieszkania", a.nr_budynku AS "Nr.Budynku",
			a.nr_telefonu AS "Nr. Telefonu",a.email
		from schemat.klient k, schemat.adres a
		where k.adresid = a.id
		order by k.id;
		
---select * from pokaz_klientow; przyklad uzycia

CREATE OR REPLACE VIEW Max_pensja AS 
	select MAX(Pensja) AS "MAX" from schemat.pracownicy;

-- select * from Max_pensja

CREATE OR REPLACE VIEW Min_pensja AS
	select MIN(Pensja) AS "MIN" from schemat.pracownicy;

-- select * from Min_pensja

CREATE OR REPLACE VIEW Srednia_pensja AS
	SELECT (AVG(Pensja::numeric)::money) AS "SREDNIA" from schemat.pracownicy;
--drop view Srednia_pensja;
--select * from Srednia_pensja;

CREATE OR REPLACE VIEW Liczba_pracownikow AS
	select COUNT(*) AS "Liczba Pracownikow" from schemat.pracownicy;
--select * from Liczba_pracownikow;

CREATE OR REPLACE VIEW Liczba_pracownikow_na_urlopie AS
	SELECT COUNT(*) AS "Liczba Pracownikow na urlopie" from schemat.pracownicy where urlop IS TRUE;
-- select * from Liczba_pracownikow_na_urlopie;

CREATE OR REPLACE VIEW Liczba_aktywnych_pracownikow AS
	SELECT COUNT(*) AS "Liczba pracujacych obecnie pracownikow" from schemat.pracownicy where urlop IS NOT TRUE;
-- select * from Liczba_aktywnych_pracownikow;
CREATE OR REPLACE VIEW Pracownik AS
	SELECT p.id, p.imie, p.nazwisko, k.nazwa_kadry AS "kadra", a.Miasto AS "Miasto zamieszkania", 
			a.kod_pocztowy AS "Kod pocztowy", a.Ulica AS "Ulica", a.nr_mieszkania AS "Nr. Mieszkania", 
			a.nr_budynku AS "Nr.Budynku", a.nr_telefonu AS "Nr. Telefonu",a.email,
			p.pensja,p.stanowisko, p.urlop, p.koniec_urlopu, p.kadry_id, p.adres_id
	FROM schemat.pracownicy p, schemat.adres a,schemat.kadry k
	where k.id = p.kadry_id
	and p.adres_id = a.id
	ORDER BY p.id;
	
-- select * from Pracownik;


CREATE OR REPLACE VIEW simple_pracownik AS
	SELECT p.id AS "ID",p.imie, p.nazwisko,p.stanowisko, a.nr_telefonu,a.email
	FROM schemat.pracownicy p, schemat.adres a
	where p.adres_id = a.id
	ORDER BY p.id;
-- select * from simple_pracownik;

CREATE OR REPLACE VIEW zlecone_zamowienia AS 
	SELECT z.id,z.komentarz, kl.imie, kl.nazwisko, a.ulica, a.nr_mieszkania AS "Nr. Mieszkania", 
		a.nr_budynku AS "Nr. Budynku", a.miasto, a.kod_pocztowy AS "Kod Pocztowy",
		a.nr_telefonu AS "Nr. Telefonu ",a.email, ku.id AS "Nr kuchni", 
		p.nazwa_produktu "Nazwa Produktu", p.cena_produktu AS "Cena Produktu",
		p.koszt_produktu AS "Koszt Detaliczny" ,p.dostepny
	FROM schemat.zamowienie z, schemat.kuchnia ku, 
		schemat.adres a,schemat.klient kl, schemat.produkt p
	WHERE z.kuchniaid = ku.id
		AND z.klientid = kl.id
		AND z.produktid = p.id
		AND kl.adresid = a.id
		AND z.stan_zamowienia = 0
	ORDER BY z.id;

CREATE OR REPLACE VIEW wykonywane_zamowienia AS 
	SELECT z.id, z.komentarz,kl.imie, kl.nazwisko, a.ulica, a.nr_mieszkania AS "Nr. Mieszkania", 
		a.nr_budynku AS "Nr. Budynku", a.miasto, a.kod_pocztowy AS "Kod Pocztowy",
		a.nr_telefonu AS "Nr. Telefonu ",a.email, ku.id AS "Nr kuchni", 
		p.nazwa_produktu "Nazwa Produktu", p.cena_produktu AS "Cena Produktu",
		p.koszt_produktu AS "Koszt Detaliczny" ,p.dostepny
	FROM schemat.zamowienie z, schemat.kuchnia ku, 
		schemat.adres a,schemat.klient kl, schemat.produkt p
	WHERE z.kuchniaid = ku.id
		AND z.klientid = kl.id
		AND z.produktid = p.id
		AND kl.adresid = a.id
		AND z.stan_zamowienia > 0
		AND z.stan_zamowienia < 3
	ORDER BY z.id;

--select * from wykonywane_zamowienia;


CREATE OR REPLACE VIEW view_produkt AS 
	SELECT id AS "ID PRODUKTU", nazwa_produktu AS "Nazwa Produktu ", ile AS "Ilosc na stanie",cena_produktu AS "Cena Produktu",
		koszt_produktu AS "Koszt Produktu", dostepny
		FROM schemat.produkt
		ORDER BY id;

CREATE OR REPLACE VIEW view_towar AS
	SELECT nazwa_towaru AS "Nazwa Towaru ", cena_towaru AS "Nazwa Towaru",
		ilosc,spozywczy
		FROM schemat.towar
		ORDER BY id;

----- select * from view_towar;
--