-- PRZYJMUJE (ULICE,nr_mieszkania,nr_budynku,MIASTO,KOD POCZTOWY,TELEFON EMAIL)
CREATE OR REPLACE FUNCTION dodaj_adres(ulica text,nr_mieszkania int, nr_budynku int,nazwa_miasta 
				text, kod_pocztowy int,nr_telefonu int,email text)
	RETURNS int LANGUAGE plpgsql AS $$
DECLARE 
one int;
ul varchar(120);
m int;
b int;
Mi varchar(120);
kot int;
t int;
e varchar(255);
BEGIN
	one  = (SELECT MAX(id) FROM SCHEMAT.adres)+1;
	ul = $1;
	m = $2;
	b = $3;
	Mi = $4;
	kot = $5;
	t = $6;
	e = $7;
	insert into SCHEMAT.adres values (one,ul,m,b,Mi,kot,t,e);
	return one;
END
$$;
--drop function dodaj_adres(text,int,int,text,int,int,int,text)
-- przyklad_uzycia SELECT * FROM dodaj_adres('x',1,1,'a',1,1,'x');
-- SELECT * FROM SCHEMAT.adres;


-- PRZYJMUJE imie, nazwisko,kase, stanowisko WERSJA BEZ URLOPUUUU!!!!!!
CREATE OR REPLACE FUNCTION dodaj_pracownika(imi text, nazwisk text, pensj numeric,stanowi int,
		/*adres*/ulica text,nr_mieszkania int, nr_budynku int,nazwa_miasta 
				text, kod_pocztowy int,nr_telefonu int,email text, /*end adres*/
										nr_kadry int)
	RETURNS void LANGUAGE plpgsql AS $$
DECLARE 
one int; -- indeks pk
ad int;
BEGIN
	one = (SELECT MAX(id) FROM SCHEMAT.pracownicy)+1;
	ad = (SELECT * FROM dodaj_adres($5,$6,$7,$8,$9,$10,$11));
	insert into SCHEMAT.pracownicy values (one,$1,$2,$3::money,$4,ad,$12,false);
END
$$; 
--SELECT * FROM dodaj_pracownika('a'::text,'a'::text,1200.00,2,'a'::text,1,1,'a'::text,1,1,'a'::text,1);
--SELECT * FROM SCHEMAT.pracownicy;
-- WERSJA Z URLOPEM!!!!!!
CREATE OR REPLACE FUNCTION dodaj_pracownika(imi text, nazwisk text, pensj numeric,stanowi int,
		/*adres*/ulica text,nr_mieszkania int, nr_budynku int,nazwa_miasta 
				text, kod_pocztowy int,nr_telefonu int,email text, /*end adres*/
										nr_kadry int,koniec_urlop date)
	RETURNS void LANGUAGE plpgsql AS $$
DECLARE 
one int; -- indeks pk
ad int;
BEGIN
	one = (SELECT MAX(id) FROM SCHEMAT.pracownicy)+1;
	ad = (SELECT * FROM dodaj_adres($5,$6,$7,$8,$9,$10,$11));
	insert into SCHEMAT.pracownicy values (one,$1,$2,$3::money,$4,ad,$12,true,$13);
END
$$; 

CREATE OR REPLACE FUNCTION usun_pracownika(i text, n text, e text)
	RETURNS void LANGUAGE PLPGSQL AS $$
DECLARE
one int;
BEGIN
	one = (select p.id 
				from schemat.pracownicy p, schemat.adres a 
				WHERE p.imie = $1 
					AND  p.nazwisko = $2 
					AND p.adres_id = a.id 
					AND a.email = $3
					limit 1);
	
	DELETE FROM SCHEMAT.pracownicy WHERE id = one;
	
END
$$;	
-- SELECT * FROM usun_pracownika('Artur','Adamek','eweq@ex.pl');
-- SELECT * FROM SCHEMAT.pracownicy;

CREATE OR REPLACE FUNCTION dynamic_update(nazwa_t text,index int,nazwa_k text, war int )
	RETURNS void LANGUAGE PLPGSQL AS $$
BEGIN
	EXECUTE 'UPDATE schemat.' || $1 || ' set ' || $3 || ' = ' || $4 || ' where id = ' || $2 || ' ;';
END
$$;

CREATE OR REPLACE FUNCTION dynamic_update(nazwa_t text,index int,nazwa_k text, war boolean )
	RETURNS void LANGUAGE PLPGSQL AS $$
BEGIN
	EXECUTE 'UPDATE schemat.' || $1 || ' set ' || $3 || ' = ' || $4 || ' where id = ' || $2 || ' ;';
END
$$;

CREATE OR REPLACE FUNCTION dynamic_update(nazwa_t text,index int,nazwa_k text, war numeric )
	RETURNS void LANGUAGE PLPGSQL AS $$
BEGIN
	EXECUTE 'UPDATE schemat.' || $1 || ' set ' || $3 || ' = ' || $4 || ' where id = ' || $2::money || ' ;';
END
$$;

-- I - ID zamowienia prior na jaki status

CREATE OR REPLACE FUNCTION zmien_status_zamowienia(i int,prior int)
	RETURNS void LANGUAGE PLPGSQL AS $$
BEGIN
	UPDATE schemat.zamowienie SET stan_zamowienia = $2 WHERE id = $1;
END
$$;

CREATE OR REPLACE FUNCTION zmien_komentarz_zamowienia(i int,k text)
	RETURNS void LANGUAGE PLPGSQL AS $$
BEGIN
	UPDATE schemat.zamowienie SET komentarz = $2 WHERE id = $1;
END
$$;

CREATE OR REPLACE FUNCTION zmien_liczbe_produktow(index int,ile int)
	RETURNS void LANGUAGE PLPGSQL AS $$
BEGIN
	update schemat.produkt set ile = $2 where id = $1;
END
$$;


--- select
CREATE OR REPLACE function co_zamowil_po_id(parm1 int)
        returns table (id int, cena money,stan_zamowienia int, nazwa_produktu text,
		cena_produktu money,koszt_produktu money,id_przepisu int)
        AS 
        $body$
        SELECT z.id AS "ID zamowienia", z.cena AS "Cena zamowienia", z.stan_zamowienia AS "Stan zamowienia",
			p.nazwa_produktu,p.cena_produktu,p.koszt_produktu,p.przepis_id AS "ID Przepisu"
		FROM schemat.zamowienie z,schemat.produkt p
				WHERE z.klientid = $1
				AND z.produktid = p.id
		ORDER BY z.id;
                        
        $body$
language sql;

CREATE OR REPLACE function co_zamowil(im text, na text,e text)
        returns table (id int, cena money,stan_zamowienia int, nazwa_produktu text,
		cena_produktu money,koszt_produktu money,id_przepisu int)
        AS 
        $body$
        SELECT z.id AS "ID zamowienia", z.cena AS "Cena zamowienia", z.stan_zamowienia AS "Stan zamowienia",
			p.nazwa_produktu,p.cena_produktu,p.koszt_produktu,p.przepis_id AS "ID Przepisu"
		FROM schemat.zamowienie z,schemat.produkt p
				WHERE z.klientid = (
						SELECT k.id FROM  schemat.klient k, schemat.adres a
						WHERE k.imie = $1
						AND k.nazwisko = $2
						AND a.id = k.adresid
						AND a.email = $3
					)
				AND z.produktid = p.id
		ORDER BY z.id;
                        
        $body$
language sql;

CREATE OR REPLACE FUNCTION wyswietl_produkt (i int)
	RETURNS TABLE (id int,nazwa_produktu text,cena money,dostepny boolean)
	AS
	$BODY$
	
	SELECT id,nazwa_produktu,cena_produktu,dostepny FROM SCHEMAT.PRODUKT
		WHERE id = $1;
	$BODY$
LANGUAGE SQL;

--select * from wyswietl_produkt(1);

-- Przyjmujemy id kuchni (domyslnie mamy tylko jedna kuchnie o id 1),id klienta, id produktu 
CREATE OR REPLACE FUNCTION zloz_zamowienie (ik int,indk int, ip int)
	RETURNS void LANGUAGE plpgsql AS $$
DECLARE 
index int;
kosz money;
BEGIN
	index = (SELECT MAX(id) FROM schemat.zamowienie) + 1;
	kosz = (SELECT koszt_produktu from schemat.produkt where id = $3);
	INSERT INTO SCHEMAT.zamowienie VALUES (index,$1,$2,$3,kosz,0,'');
	
END
$$;
--SELECT * FROM zloz_zamowienie(1,1,1);
-- imie nazwisko i adres!
CREATE OR REPLACE FUNCTION nowy_klient (im text,naz text,
			/*adres*/ulica text,nr_mieszkania int, nr_budynku int,nazwa_miasta 
				text, kod_pocztowy int,nr_telefonu int,email text) /*end adres*/
RETURNS void LANGUAGE PLPGSQL AS $$
DECLARE
ad int;
ind int;
BEGIN
	ad = (SELECT * FROM dodaj_adres($3,$4,$5,$6,$7,$8,$9));
	ind = (SELECT MAX(id) FROM SCHEMAT.klient) + 1;
	INSERT INTO SCHEMAT.KLIENT VALUES (ind,$1,$2,ad);
END
$$;
select * from nowy_klient('Bartosz','Bargiel','kocia',0,9,'Olkusz',4343,4343,'xxx@oniet.pl');


----