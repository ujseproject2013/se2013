#include "dialog.h"
#include "ui_dialog.h" 
#include "managerwindow.h"
#include <QString>
#include "string"
#include "fstream"
Dialog::Dialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Dialog)
{
    ui->setupUi(this);
    isloadedT=false;
}
Dialog::Dialog(ManagerWindow *parent) :
    QDialog(parent),
    ui(new Ui::Dialog)
{
    ui->setupUi(this);
    this->parent=parent;
    kadryID = 0;
    adresID = 0;
    pracoID = 0;
    isloadedT=false;
    imie = false;
    nazwisko = false;
    ulica = false;
    nrdomu = false;
    nrmiesz = false;
    kod = false;
    miasto  = false;
    nrtel = false;
    mailb = false;
    nrkadry = false;
    data  = false;
    pensja  = false;
    nrstan = false;
}
Dialog::~Dialog()
{
    delete ui;
}
void Dialog::connectWithIndex(const QModelIndex  &i)
{
    this->index=i;
}

void Dialog::loadDataToForm()
{
        isloadedT=true;
        int k = this->index.row();
        parent->ui->workerstable->selectRow(k);

        QItemSelectionModel* list = parent->ui->workerstable->selectionModel();
        pracoID = list->selectedIndexes().at(0).data().toInt();
        kadryID = list->selectedIndexes().at(15).data().toInt();
        adresID = list->selectedIndexes().at(16).data().toInt();
        ui->imie->insertPlainText(list->selectedIndexes().at(1).data().toString()); this->name = list->selectedIndexes().at(1).data().toString();
        ui->nazwisko->insertPlainText(list->selectedIndexes().at(2).data().toString()); this->nazw = list->selectedIndexes().at(2).data().toString();
        ui->pensja_2->insertPlainText(list->selectedIndexes().at(11).data().toString());
        ui->idstanow->insertPlainText(list->selectedIndexes().at(12).data().toString());
        ui->ulica->insertPlainText(list->selectedIndexes().at(6).data().toString());
        ui->nrdomu->insertPlainText(list->selectedIndexes().at(8).data().toString());
        ui->nrmiesz->insertPlainText(list->selectedIndexes().at(7).data().toString());
        ui->miasto_2->insertPlainText(list->selectedIndexes().at(4).data().toString());
        ui->kodpoczt->insertPlainText(list->selectedIndexes().at(5).data().toString());
        ui->telefon->insertPlainText(list->selectedIndexes().at(9).data().toString());
        ui->mail->insertPlainText(list->selectedIndexes().at(10).data().toString()); this->mail = list->selectedIndexes().at(10).data().toString();
        ui->nazwakadry->insertPlainText(list->selectedIndexes().at(3).data().toString());
        ui->dataprzy->insertPlainText(list->selectedIndexes().at(14).data().toString());
        ui->nrkadr->insertPlainText(list->selectedIndexes().at(15).data().toString());
        imie = false;
        nazwisko = false;
        ulica = false;
        nrdomu = false;
        nrmiesz = false;
        kod = false;
        miasto  = false;
        nrtel = false;
        mailb = false;
        nrkadry = false;
        data  = false;
        pensja  = false;
        nrstan = false;

}

void Dialog::on_close_clicked()
{
    this->QDialog::close();
}

void Dialog::on_exportToTxt_clicked()
{
    std::string path= ui->imie->toPlainText().toStdString()+"_"+ui->nazwisko->toPlainText().toStdString()+".txt";
    std::ofstream exportfile(path.c_str());
                                          exportfile <<ui->imie->toPlainText().toStdString()<<std::endl;
                                          exportfile <<ui->nazwisko->toPlainText().toStdString()<<std::endl;
                                          exportfile <<ui->pensja_2->toPlainText().toStdString()<<std::endl;
                                          exportfile <<ui->idstanow->toPlainText().toStdString()<<std::endl;
                                          exportfile <<ui->ulica->toPlainText().toStdString()<<std::endl;
                                          exportfile <<ui->nrdomu->toPlainText().toStdString()<<"/"<<ui->nrmiesz->toPlainText().toStdString()<<std::endl;
                                          exportfile <<ui->miasto_2->toPlainText().toStdString()<<std::endl;
                                          exportfile <<ui->kodpoczt->toPlainText().toStdString()<<std::endl;
                                          exportfile <<ui->telefon->toPlainText().toStdString()<<std::endl;
                                          exportfile <<ui->mail->toPlainText().toStdString()<<std::endl;
                                          exportfile <<ui->nazwakadry->toPlainText().toStdString()<<" "<<ui->nrkadr->toPlainText().toStdString()<<std::endl;
                                          exportfile <<ui->dataprzy->toPlainText().toStdString()<<std::endl;
                                          exportfile.close();
}
bool Dialog::isloaded()
{
    if(isloadedT) return true; return false;
}

void Dialog::on_accep_clicked() //not working
{
    QString a = ui->pensja_2->toPlainText();
    QString b = "";
    int i = 0;
    while(a[i]!=',')
    {
        if(a[i]!=' ') b += a[i]; i++;
    }
    if(isloaded())
    {
        if(imie) parent->database->updateStringVal("pracownicy","imie",ui->imie->toPlainText(),pracoID);
        if(nazwisko) parent->database->updateStringVal("pracownicy","nazwisko",ui->nazwisko->toPlainText(),pracoID);
        if(ulica) parent->database->updateStringVal("adres","ulica",ui->ulica->toPlainText(),adresID);
        if(nrdomu) parent->database->updateIntVal("adres","nr_budynku",ui->nrdomu->toPlainText().toInt(),adresID);
        if(nrmiesz) parent->database->updateIntVal("adres","nr_mieszkania",ui->nrmiesz->toPlainText().toInt(),adresID);
        if(kod) parent->database->updateIntVal("adres","kod_pocztowy",ui->kodpoczt->toPlainText().toInt(),adresID);
        if(miasto) parent->database->updateStringVal("adres",",miasto",ui->miasto_2->toPlainText(),adresID);
        if(nrtel) parent->database->updateIntVal("adres","nr_telefonu",ui->telefon->toPlainText().toInt(),adresID);
        if(mailb) parent->database->updateStringVal("adres","email",ui->mail->toPlainText(),adresID);
        if(nrkadry) parent->database->updateIntVal("pracownicy","kadry_id",ui->nrkadr->toPlainText().toInt(),pracoID);
        if(data) parent->database->updateDateVal("pracownicy","koniec_urlopu",ui->dataprzy->toPlainText(),pracoID);
        if(pensja) parent->database->updateIntVal("pracownicy","pensja",b.toInt(),pracoID);
        if(nrstan) parent->database->updateIntVal("pracownicy","stanowisko",ui->idstanow->toPlainText().toInt(),pracoID);
    }
    else
    {
        parent->database->addEmployee(ui->imie->toPlainText().toStdString().c_str(),
                                      ui->nazwisko->toPlainText().toStdString().c_str(),
                                      b.toStdString().c_str(),
                                      ui->idstanow->toPlainText().toInt(),
                                      ui->ulica->toPlainText().toStdString().c_str(),
                                      ui->nrmiesz->toPlainText().toInt(),
                                      ui->nrdomu->toPlainText().toInt(),
                                      ui->miasto_2->toPlainText().toStdString().c_str(),
                                      ui->kodpoczt->toPlainText().toInt(),
                                      ui->telefon->toPlainText().toInt(),
                                      ui->mail->toPlainText().toStdString().c_str(),
                                      ui->nrkadr->toPlainText().toInt(),NULL);
    }
    this->parent->refresh();
    this->QDialog::close();
}


void Dialog::on_imie_textChanged()
{
    imie=true;
}

void Dialog::on_nazwisko_textChanged()
{
    nazwisko=true;
}

void Dialog::on_ulica_textChanged()
{
    ulica=true;
}

void Dialog::on_nrdomu_textChanged()
{
    nrdomu=true;
}

void Dialog::on_nrmiesz_textChanged()
{
    nrmiesz=true;
}

void Dialog::on_kodpoczt_textChanged()
{
    kod=true;
}

void Dialog::on_miasto_2_textChanged()
{
    miasto=true;
}

void Dialog::on_telefon_textChanged()
{
    nrtel=true;
}

void Dialog::on_mail_textChanged()
{
    mailb=true;
}

void Dialog::on_nrkadr_textChanged()
{
    nrkadry=true;
}

void Dialog::on_dataprzy_textChanged()
{
    data=true;
}

void Dialog::on_pensja_2_textChanged()
{
    pensja=true;
}

void Dialog::on_idstanow_textChanged()
{
    nrstan=true;
}
