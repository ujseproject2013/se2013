#ifndef DIALOG_H
#define DIALOG_H
#include "managerwindow.h"
#include <QDialog>
#include <QtWidgets>
namespace Ui {
class Dialog;
}

class Dialog : public QDialog
{
    Q_OBJECT
    
public:
    explicit Dialog(QWidget *parent = 0);
    explicit Dialog(ManagerWindow *parent);
    ~Dialog();
    void connectWithIndex(const QModelIndex &i);
    void loadDataToForm();
    bool isloaded();
private slots:
    void on_close_clicked();

    void on_exportToTxt_clicked();

    void on_accep_clicked();

    void on_imie_textChanged();

    void on_nazwisko_textChanged();

    void on_ulica_textChanged();

    void on_nrdomu_textChanged();

    void on_nrmiesz_textChanged();

    void on_kodpoczt_textChanged();

    void on_miasto_2_textChanged();

    void on_telefon_textChanged();

    void on_mail_textChanged();

    void on_nrkadr_textChanged();

    void on_dataprzy_textChanged();

    void on_pensja_2_textChanged();

    void on_idstanow_textChanged();

private:
    Ui::Dialog *ui;
    ManagerWindow * parent;
    QModelIndex index;
    bool isloadedT;
    QString name;
    QString nazw;
    QString mail;
    bool imie;
    bool nazwisko;
    bool ulica;
    bool nrdomu;
    bool nrmiesz;
    bool kod;
    bool miasto;
    bool nrtel;
    bool mailb;
    bool nrkadry;
    bool data;
    bool pensja;
    bool nrstan;
    int kadryID;
    int adresID;
    int pracoID;
};

#endif // DIALOG_H
