#ifndef DIALOGYN_H
#define DIALOGYN_H
#include "managerwindow.h"
#include <QDialog>
#include <QString>
namespace Ui {
class DialogYN;
}

class DialogYN : public QDialog
{
    Q_OBJECT
    
public:
    explicit DialogYN(QWidget *parent = 0, QString name ="");
    DialogYN(ManagerWindow * parent = 0, QString name="", QString surname ="", QString email ="");
    ~DialogYN();
    void connectWithIndex(const QModelIndex &i);
private slots:
    void on_y_clicked();

    void on_pushButton_2_clicked();

private:
    Ui::DialogYN *ui;
    ManagerWindow * parent;
    QModelIndex index;
    QString n;
    QString s;
    QString e;
};

#endif // DIALOGYN_H
