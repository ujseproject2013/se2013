#include "managerwindow.h"
#include <QtWidgets/QApplication>
#include <QFile>
#include "fstream"
#include <iostream>
void loadData(std::string table [],BinderHandler * database)
{
    database->SetHost(table[0].c_str());
    database->SetPort(table[1].c_str());
    database->SetOptions(table[2].c_str());
    database->SetTTY(table[3].c_str());
    database->SetDBName(table[4].c_str());
    database->SetLogin(table[5].c_str());
    database->SetPWD(table[6].c_str());
}

int main(int argc, char *argv[])
{
	QApplication a(argc, argv);
	ManagerWindow w;
    w.database = new BinderHandler();
    /*****************************************/
    std::string path = "data.txt";
    std::ifstream o (path.c_str());
    std::string text;
    std::string data [7];
    std::string answer="";
    for(int i=0;i<6;i++) //przeczytanie reszty danych
    {
        getline(o,text);
        if(text=="NULL") text="";
        data[i]=text;
    }
    getline(o,text); // pobranie zakodowanego hasła
    char move = text[0];
    for(int i=1; i<text.length(); i++)
    {
        char temp = text[i];
        temp-=move;
        answer+=temp;
    }
    data[6]=answer;
    o.close();
    loadData(data,w.database);
    /*****************************************/


	w.show();

    w.refresh();
	return a.exec();
}
