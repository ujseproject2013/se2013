#ifndef MANAGERWINDOW_H
#define MANAGERWINDOW_H
#include <QtWidgets/QMainWindow>
#include "ui_managerwindow.h"
#include "gui-binder.h"
class ManagerWindow : public QMainWindow
{
	Q_OBJECT

public:
	ManagerWindow(QWidget *parent = 0);
	~ManagerWindow();
    void refresh();
    BinderHandler * database;
    Ui::MainWindow *ui;
private slots:
	void on_refresh_clicked();
	void on_addemployee_clicked();
	void on_removeemployee_clicked();
    void on_actionZamknij_triggered();

    void on_workerstable_doubleClicked(const QModelIndex &index);

private:


};
#endif // MANAGERWINDOW_H
