#include "dialogyn.h"
#include "ui_dialogyn.h"

DialogYN::DialogYN(QWidget *parent, QString name) :
    QDialog(parent),
    ui(new Ui::DialogYN)
{
    ui->setupUi(this);
    ui->name->setText(name);
}
DialogYN::DialogYN(ManagerWindow *parent, QString name, QString surname, QString email) :
    QDialog(parent),
    ui(new Ui::DialogYN)
{
    ui->setupUi(this);
    this->parent=parent;
    this->setWindowTitle("Potwierdź usunięcie");
    ui->name->setText(name+ " "+ surname);
    n=name;
    s=surname;
    e=email;
}
DialogYN::~DialogYN()
{
    delete ui;
}
void DialogYN::connectWithIndex(const QModelIndex &i)
{
    this->index=i;
}

void DialogYN::on_y_clicked() // not working
{
    this->parent->database->deleteEmployee(n.toStdString().c_str(),s.toStdString().c_str(),e.toStdString().c_str());
    this->parent->refresh();
    this->close();
}

void DialogYN::on_pushButton_2_clicked()
{
    this->close();
}
