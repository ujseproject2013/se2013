#include "managerwindow.h"
#include "dialog.h"
#include "dialogyn.h"
#include <iostream>
ManagerWindow::ManagerWindow(QWidget *parent)
	: QMainWindow(parent),
	ui(new Ui::MainWindow)
{
	ui->setupUi(this);
}
void ManagerWindow::on_refresh_clicked()
{

    database->FillTableView(ui->workerstable,"Pracownik");
    ui->maxpay->clear();
    ui->minpay->clear();
    ui->avgpay->clear();
    ui->noofworkers->clear();
    ui->noworkingnow->clear();
    ui->noinweekend->clear();
    ui->maxpay->insertPlainText(database->getValueFromCmd("Max_pensja"));
    ui->minpay->insertPlainText(database->getValueFromCmd("Min_pensja"));
    ui->avgpay->insertPlainText(database->getValueFromCmd("Srednia_pensja"));
    ui->noofworkers->insertPlainText(database->getValueFromCmd("Liczba_pracownikow"));
    ui->noinweekend->insertPlainText(database->getValueFromCmd("Liczba_pracownikow_na_urlopie"));
    ui->noworkingnow->insertPlainText(database->getValueFromCmd("Liczba_aktywnych_pracownikow"));

}
void ManagerWindow::refresh()
{
    this->on_refresh_clicked();
}

void ManagerWindow::on_addemployee_clicked()
{
    Dialog * Employee = new Dialog(this);
    Employee->QDialog::setWindowTitle("Dodaj pracownika");
    Employee->QDialog::show();
}
void ManagerWindow::on_removeemployee_clicked()
{
    int k = this->ui->workerstable->currentIndex().row();
    ui->workerstable->selectRow(k);
    QItemSelectionModel* list = ui->workerstable->selectionModel();
    QString name = list->selectedIndexes().at(1).data().toString();
    QString surname = list->selectedIndexes().at(2).data().toString();
    QString email = list->selectedIndexes().at(10).data().toString();
    DialogYN * confirm = new DialogYN(this,name,surname,email);
    confirm->QDialog::show();
}
ManagerWindow::~ManagerWindow()
{
    delete database; // może powodować crashe
	delete ui;
}

void ManagerWindow::on_actionZamknij_triggered()
{
    this->close();
}

void ManagerWindow::on_workerstable_doubleClicked(const QModelIndex &index)
{
    Dialog * Employee = new Dialog(this);
    Employee->setWindowTitle("Edytuj pracownika");
    Employee->connectWithIndex(index);
    Employee->loadDataToForm();
    Employee->show();
}

