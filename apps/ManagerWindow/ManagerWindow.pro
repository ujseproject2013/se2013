#-------------------------------------------------
#
# Project created by QtCreator 2013-06-02T12:48:30
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = ManagerWindow
TEMPLATE = app


SOURCES += main.cpp\
        managerwindow.cpp \
    gui-binder.cpp \
    dialog.cpp \
    dialogyn.cpp

HEADERS  += managerwindow.h \
    dialog.h \
    dialogyn.h

FORMS    += managerwindow.ui \
    dialog.ui \
    dialogyn.ui

win32: LIBS += -L$$PWD/../pqsql/x86/lib/ -llibpq

INCLUDEPATH += $$PWD/../pqsql/x86/include
DEPENDPATH += $$PWD/../pqsql/x86/include

win32: PRE_TARGETDEPS += $$PWD/../pqsql/x86/lib/libpq.lib
