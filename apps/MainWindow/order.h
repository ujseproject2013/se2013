#ifndef ORDER_H
#define ORDER_H

#include <QDialog>
#include "mainwindow.h"
namespace Ui {
class Order;
}

class Order : public QDialog
{
    Q_OBJECT
    
public:
    explicit Order(QWidget *parent = 0);
    ~Order();
    explicit Order(MainWindow *parent);
    void connectWithIndex(const QModelIndex &i);
    void loadDataToForm();
    
private slots:
    void on_Przyjmij_clicked();

    void on_cancelO_clicked();

    void on_closeit_clicked();

private:
    Ui::Order *ui;
    MainWindow * parent;
    QModelIndex index;
    QItemSelectionModel* list;
};

#endif // ORDER_H
