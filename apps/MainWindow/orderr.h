#ifndef ORDERR_H
#define ORDERR_H

#include <QDialog>
#include "mainwindow.h"
namespace Ui {
class OrderR;
}

class OrderR : public QDialog
{
    Q_OBJECT
    
public:
    explicit OrderR(QWidget *parent = 0);
    ~OrderR();
    explicit OrderR(MainWindow *parent);
    void connectWithIndex(const QModelIndex &i);
    void loadDataToForm();
private slots:
    void on_exit_clicked();

    void on_cancelit_clicked();

    void on_sendit_clicked();

private:
    Ui::OrderR *ui;
    MainWindow * parent;
    QModelIndex index;
    QItemSelectionModel* list;
};

#endif // ORDERR_H
