#include "order.h"
#include "ui_order.h"
#include <QString>
Order::Order(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Order)
{
    ui->setupUi(this);
}

Order::~Order()
{
    delete ui;
}
void Order::loadDataToForm()
{
    int k = this->index.row();
    parent->ui->ordersawait->selectRow(k);
    list = parent->ui->ordersawait->selectionModel();
    ui->imienazwisko->insertPlainText(list->selectedIndexes().at(2).data().toString()+" "+list->selectedIndexes().at(3).data().toString());
    ui->ulica->insertPlainText(list->selectedIndexes().at(4).data().toString());
    ui->nrdomuim->insertPlainText(list->selectedIndexes().at(6).data().toString()+"\\"+list->selectedIndexes().at(5).data().toString());
    ui->kodimiasto->insertPlainText(list->selectedIndexes().at(8).data().toString()+" "+list->selectedIndexes().at(7).data().toString());
    ui->product->insertPlainText(list->selectedIndexes().at(12).data().toString());
    ui->warnings->insertPlainText(list->selectedIndexes().at(1).data().toString());
    ui->telefon->insertPlainText(list->selectedIndexes().at(9).data().toString());
}
void Order::connectWithIndex(const QModelIndex  &i)
{
    this->index=i;
}
Order::Order(MainWindow *parent) :
    QDialog(parent),
    ui(new Ui::Order)
{
    ui->setupUi(this);
    this->parent=parent;
}

void Order::on_Przyjmij_clicked()
{
    parent->database->updateStatus(list->selectedIndexes().at(0).data().toString().toInt(),1);
    parent->Refresh();
    this->close();
}

void Order::on_cancelO_clicked()
{
    parent->database->updateStatus(list->selectedIndexes().at(0).data().toString().toInt(),4);
    parent->Refresh();
    this->close();
}

void Order::on_closeit_clicked()
{
    this->close();
}
