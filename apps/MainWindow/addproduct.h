#ifndef ADDPRODUCT_H
#define ADDPRODUCT_H

#include <QDialog>
#include "mainwindow.h"
namespace Ui {
class Addproduct;
}

class Addproduct : public QDialog
{
    Q_OBJECT
    
public:
    explicit Addproduct(QWidget *parent = 0);
    explicit Addproduct(MainWindow *parent);
    ~Addproduct();
    
private slots:
    void on_zamk_clicked();

    void on_add_clicked();

    void on_textEdit_textChanged();

    void on_productlist_clicked(const QModelIndex &index);

private:
    Ui::Addproduct *ui;
    MainWindow * parent;
    QModelIndex index;
    QItemSelectionModel* list;
};

#endif // ADDPRODUCT_H
