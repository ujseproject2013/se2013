#include "mainwindow.h"
#include "order.h"
#include "orderr.h"
#include "gui-binder.h"
#include "addproduct.h"
MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
}
MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_actionZamknij_triggered()
{
    this->close();
}
void MainWindow::Refresh()
{
    on_actionOd_wie_triggered();
}

void MainWindow::on_actionOd_wie_triggered()
{
    database->FillTableView(ui->ordersawait,"zlecone_zamowienia");
    database->FillTableView(ui->ordersaccept,"wykonywane_zamowienia");
    ui->ordersaccept->selectColumn(0);
    int OW = ui->ordersawait->model()->rowCount();
    ui->waiting->display(QString::number(OW));
    int OA = ui->ordersaccept->model()->rowCount();
    ui->todo->display(QString::number(OA));
}

void MainWindow::on_actionDodaj_produkt_do_magazynu_triggered()
{
    Addproduct * dialog = new Addproduct (this);
    dialog->show();
}

void MainWindow::on_ordersawait_clicked(const QModelIndex &index)
{
    Order *dialog =new Order(this);
    dialog->connectWithIndex(index);
    dialog->loadDataToForm();
    dialog->show();
}

void MainWindow::on_ordersaccept_clicked(const QModelIndex &index)
{
    OrderR * dialog = new OrderR(this);
    dialog->connectWithIndex(index);
    dialog->loadDataToForm();
    dialog->show();
}
