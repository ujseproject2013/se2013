#include "addproduct.h"
#include "ui_addproduct.h"

Addproduct::Addproduct(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Addproduct)
{
    ui->setupUi(this);
}
Addproduct::Addproduct(MainWindow *parent) :
    QDialog(parent),
    ui(new Ui::Addproduct)
{
    ui->setupUi(this);
    this->parent=parent;
    this->parent->database->FillTableView(ui->productlist,"view_produkt");
}
Addproduct::~Addproduct()
{
    delete ui;
}

void Addproduct::on_zamk_clicked()
{
    this->close();
}

void Addproduct::on_add_clicked()
{
    int k = this->ui->productlist->currentIndex().row();
    ui->productlist->selectRow(k);
    list = ui->productlist->selectionModel();
    int howmany = ui->textEdit->toPlainText().toInt();
    int already = list->selectedIndexes().at(2).data().toInt();
    int id = list->selectedIndexes().at(0).data().toInt();
    parent->database->updateilosc(id,howmany+already);
    this->close();
}

void Addproduct::on_textEdit_textChanged()
{
    ui->add->setEnabled(true);
}

void Addproduct::on_productlist_clicked(const QModelIndex &index)
{
    ui->textEdit->setEnabled(true);
}
