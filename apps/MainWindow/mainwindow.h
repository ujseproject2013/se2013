#ifndef MAINWINDOW_H
#define MAINWINDOW_H
#include <QtWidgets/QMainWindow>
#include "ui_mainwindow.h"
#include "gui-binder.h"
namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT
    
public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
    BinderHandler * database;
    void Refresh();
    Ui::MainWindow *ui;
private slots:

    void on_actionZamknij_triggered();

    void on_actionOd_wie_triggered();

    void on_actionDodaj_produkt_do_magazynu_triggered();

    void on_ordersawait_clicked(const QModelIndex &index);

    void on_ordersaccept_clicked(const QModelIndex &index);

private:

};

#endif // MAINWINDOW_H

