#include "orderr.h"
#include "ui_orderr.h"

OrderR::OrderR(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::OrderR)
{
    ui->setupUi(this);
}

OrderR::~OrderR()
{
    delete ui;
}
void OrderR::loadDataToForm()
{
    int k = this->index.row();
    parent->ui->ordersaccept->selectRow(k);
    list = parent->ui->ordersawait->selectionModel();
    bool isMaterials = false;
    if(list->selectedIndexes().at(15).data().toString().toUpper()=="T") isMaterials = true;
    ui->textEdit->setPlainText(list->selectedIndexes().at(12).data().toString());
    if(!isMaterials) ui->sendit->setEnabled(false);
}
void OrderR::connectWithIndex(const QModelIndex  &i)
{
    this->index=i;
}
OrderR::OrderR(MainWindow *parent) :
    QDialog(parent),
    ui(new Ui::OrderR)
{
    ui->setupUi(this);
    this->parent=parent;
}

void OrderR::on_exit_clicked()
{
    this->close();
}

void OrderR::on_cancelit_clicked()
{
    parent->database->updateStatus(list->selectedIndexes().at(0).data().toString().toInt(),4);
    parent->Refresh();
    this->close();
}

void OrderR::on_sendit_clicked()
{
    parent->database->updateStatus(list->selectedIndexes().at(0).data().toString().toInt(),3);
    parent->Refresh();
    this->close();
}
