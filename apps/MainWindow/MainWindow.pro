#-------------------------------------------------
#
# Project created by QtCreator 2013-06-02T12:11:45
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = MainWindow
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    gui-binder.cpp \
    order.cpp \
    orderr.cpp \
    addproduct.cpp

HEADERS  += mainwindow.h \
    order.h \
    orderr.h \
    addproduct.h

FORMS    += mainwindow.ui \
    order.ui \
    orderr.ui \
    addproduct.ui

win32: LIBS += -L$$PWD/../pqsql/x86/lib/ -llibpq

INCLUDEPATH += $$PWD/../pqsql/x86/include
DEPENDPATH += $$PWD/../pqsql/x86/include

win32: PRE_TARGETDEPS += $$PWD/../pqsql/x86/lib/libpq.lib
