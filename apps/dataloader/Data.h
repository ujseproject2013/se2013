#include <string>
#include "File.h"
#include <time.h>
#ifndef DATA_H
#define DATA_H
using namespace std;
class Data
{
public:
	Data()
	{
		this->host="";
		this->port="";
		this->options="";
		this->tty="";
		this->dbname="";
		this->login="";
		this->pwd="";
	}
	~Data()
	{
		
	}
	string * GetDataFromInput()
	{
		string * data = new string [7];
		cout << "Podaj dane zgodnie z formatem:\nHOST\nPORT\nOPTIONS\nTTY\nDBNAME\nLOGIN\nPASSWORD\nPo kazdym ciagu wcisnij ENTER\nJesli ciag ma byc pusty wpisz NULL" << endl;
		for(int i=0; i<7; i++)
		{
			cout<< i << ": "<<endl;
			cin >> data[i];
		}
		return data;
	}
	void loadDataToClass(string data[])
	{
		this->host=data[0];
		this->port=data[1];
		this->options=data[2];
		this->tty=data[3];
		this->dbname=data[4];
		this->login=data[5];
		this->pwd=data[6];
	}
	void loadDataToFile(string filename)
	{
		File * file = new File(filename);
		file->addline(this->host);
		file->addline(this->port);
		file->addline(this->options);
		file->addline(this->tty);
		file->addline(this->dbname);
		file->addline(this->login);
		file->addline(codePWD(this->pwd));
		file->~File();
	}
private:
	string codePWD(string pwd)
	{
		srand(time(NULL));
		string answer="";
		int move = rand() % 5 + 1;
		answer+=move;
		
		for(int i=0; i<pwd.length(); i++)
		{
			char temp = (char)pwd[i];
			temp+=move;
			answer+=temp;
		}
		return answer;
	}
	string host;
	string port;
	string options;
	string tty;
	string dbname;
	string login;
	string pwd;
};
#endif