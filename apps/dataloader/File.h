#include <iostream>
#include <fstream>
#include <istream>
#include <string>
#ifndef FILE_H
#define FILE_H
class File
{
public:
	File(std::string filename)
	{
		fileHandler.open(filename);
	}
	~File()
	{
		fileHandler.close();
	}
	/*
	void K()
	{
		std::ifstream o ("data.txt");
		std::string text;
		std::string answer="";
		for(int i=0;i<6;i++) getline(o,text); //przeczytanie reszty danych
		getline(o,text); // pobranie zakodowanego has�a
		char move = text[0];
		for(int i=1; i<text.length(); i++)
		{
		char temp = text[i];
		temp-=move;
		answer+=temp;
		}
		std::cout<<answer<<std::endl;
		o.close();
	}
	*/
	void addline(std::string line)
	{
		fileHandler << line <<std::endl;
	}
private:
	std::ofstream fileHandler;
};
#endif