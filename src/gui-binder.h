#ifndef GUIBINDER_H
#define GUIBINDER_H
#include "libpq-fe.h"
#include <QWidget>
#include <QTableView>
#include <QListView>
#include <QString>
#include <QMessageBox>

const char* QStrToCStr(QString qstring);
void ShowInfoBox(QString string);
QString IToQS(int x);
QString DToQS(double x);

class BinderHandler
{

private :

    PGconn* DBConnPointer;
    PGresult* DBResultPointer;

    const char *pghost;
    const char *pgport;
    const char *pgoptions;
    const char *pgtty;
    const char *dbName;
    const char *login;
    const char *pwd;

    void DBConnect();
    void DBDisconnect();
    bool isConnectionOk();
    void ShowErrWithConnection();
    void ShowErrCmdStatusBox();
    void updateRow(QString tableName, QString columnName, QString newValue, QString ID);

public :

    //---------------------------------------------

    //-------Konstruktory--------------------------

    BinderHandler();
    ~BinderHandler();

    //---------------------------------------------

    void SetHost(const char *pghost);
    void SetPort(const char *pgport);
    void SetOptions(const char *pgoptions);
    void SetTTY(const char *pgtty);
    void SetDBName(const char *dbName);
    void SetLogin(const char *login);
    void SetPWD(const char *pwd);

    //---------------------------------------------

	void updateStatus(int id, int newprio);
	void updateilosc(int id, int amount);
    void SetDBConnStrings(const char *pghost, const char *pgport,
                          const char *pgoptions, const char *pgtty,
                          const char *dbName, const char *login,
                          const char *pwd);

    void SetDBConnStrings(QString pghost, QString pgport,
                          QString pgoptions, QString pgtty,
                          QString dbName, QString login,
                          QString pwd);

    //---------------------------------------------

    //-----Metody pomocnicze-----------------------

    const char* GetStrFromTblItem(QTableView* qTableP, int column, int row);

    //---------------------------------------------

    //-----Metody ogólne, zależne od komend.-------

    void FillTableView(QTableView* TableView, const char* ViewName, QWidget* wid = NULL);
    void FillListView(QListView* ListView, const char* ViewName, const char* ColumnName );
    QString getValueFromCmd(const char *Cmd);
    void EditData(const char *Command, const char* params);

    //---------------------------------------------

    //-----Metody strikte mocno związane z bazą----

    void deleteEmployee(const char* imie, const char* nazwisko, const char* mail);
    void deleteEmployee(QString imie, QString nazwisko, QString mail);

    void addEmployee(const char* Imie,
                     const char* Nazwisko,
                     const char* Placa,
                             int idStanowiska,
                     const char* Ulica,
                             int NumerMieszkania,
                             int NumerDomu,
                     const char* Miejscowosc,
                             int KodPocztowy,
                             int NumerTel,
                     const char* Mail,
                             int NrKadry,
                     const char* Data = NULL); // FORMAT YYYY-MM-DD

    void addEmployee(QString Imie,
                     QString Nazwisko,
                         int Placa,
                         int idStanowiska,
                     QString Ulica,
                         int NumerMieszkania,
                         int NumerDomu,
                     QString Miejscowosc,
                         int KodPocztowy,
                         int NumerTel,
                     QString Mail,
                         int NrKadry,
                     QString Data); // FORMAT YYYY-MM-DD



    //---------------------------------------------

    //SELECT dodaj_pracownika('Franek','Głośny' , to_number('1200', ''), '1', 'Metalowców', 13, 23, 'Kraków', 30682, 666666666, 'example@mail.com', 1, to_date('2013-08-06', 'YYYY-MM-DD'));

    void updateStringVal(QString tableName, QString columnName, QString newValue, int ID);
    void updateDateVal(QString tableName, QString columnName, QString newValue,  int ID, QString dateFormat = "YYYY-MM-DD");
    void updateNumericVal(QString tableName, QString columnName, QString newValue, int ID);
    void updateNumericVal(QString tableName, QString columnName, double newValue, int ID);
    void updateIntVal(QString tableName, QString columnName, QString newValue, int ID);
    void updateIntVal(QString tableName, QString columnName, int newValue, int ID);



};



#endif // GUIBINDER_H
