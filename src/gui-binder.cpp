#include "gui-binder.h"
#include <QMessageBox>
#include <QTableView>
#include <QStandardItemModel>
#include <QString>
#include <QStringList>
#include <QStringListModel>
#include "libpq-fe.h"

//================================================
//===Funkcje=pomocnicze===========================
//================================================

const char* QStrToCStr(QString qstring)
{
    return qstring.toStdString().c_str();
}

void ShowInfoBox(QString string)
{
    QMessageBox msgBox;
    msgBox.setIcon(QMessageBox::Information);
    msgBox.setText(string);
    msgBox.exec();
}

QString IToQS(int x)
{
    return QString::number(x);
}

QString DToQS(double x)
{
    return QString::number(x);
}

//================================================
//===BinderHandler==Class=========================
//================================================

void BinderHandler::DBConnect()
{
    DBConnPointer = PQsetdbLogin(this->pghost,this->pgport,
                                 this->pgoptions,this->pgtty,
                                 this->dbName,this->login,this->pwd);
}

void BinderHandler::DBDisconnect()
{
    PQclear(DBResultPointer);
    PQfinish(DBConnPointer);
}

bool BinderHandler::isConnectionOk()
{
    return PQstatus(this->DBConnPointer) == CONNECTION_OK;
}

void BinderHandler::ShowErrWithConnection()
{
    QString errmsg;
    errmsg += "Error: Blad polaczenia z serverem.\n";
    errmsg += PQerrorMessage(DBConnPointer);

    QMessageBox msgBox;
    msgBox.setText(errmsg);
    msgBox.setIcon(QMessageBox::Critical);
    msgBox.exec();
}

void BinderHandler::ShowErrCmdStatusBox()
{
    QString msg;
    msg += PQresStatus(PQresultStatus(DBResultPointer));
    msg += ":\n";
    msg += PQresultErrorMessage(DBResultPointer);

    QMessageBox msgBox;
    msgBox.setText(msg);
    msgBox.setIcon(QMessageBox::Critical);
    msgBox.exec();
}

BinderHandler::BinderHandler()
{
    pghost = NULL;
    pgport = NULL;
    pgoptions = NULL;
    pgtty = NULL;
    dbName = NULL;
    login = NULL;
    pwd = NULL;
}

BinderHandler::~BinderHandler()
{
    PQclear(DBResultPointer);
}

void BinderHandler::SetDBConnStrings(const char *pghost, const char *pgport,
                                const char *pgoptions, const char *pgtty,
                                const char *dbName, const char *login,
                                const char *pwd)
{
    this->pghost = pghost;
    this->pgport = pgport;
    this->pgoptions = pgoptions;
    this->pgtty = pgtty;
    this->dbName = dbName;
    this->login = login;
    this->pwd = pwd;
}

void BinderHandler::SetDBConnStrings(QString pghost, QString pgport,
                                     QString pgoptions, QString pgtty,
                                     QString dbName, QString login,
                                     QString pwd)
{
    SetDBConnStrings(QStrToCStr(pghost), QStrToCStr(pgport),
                     QStrToCStr(pgoptions), QStrToCStr(pgtty),
                     QStrToCStr(dbName), QStrToCStr(login),
                     QStrToCStr(pwd));
}

void BinderHandler::SetHost(const char *pghost)
{this->pghost = pghost;}

void BinderHandler::SetPort(const char *pgport)
{this->pgport = pgport;}

void BinderHandler::SetOptions(const char *pgoptions)
{this->pgoptions = pgoptions;}

void BinderHandler::SetTTY(const char *pgtty)
{this->pgtty = pgtty;}

void BinderHandler::SetDBName(const char *dbName)
{this->dbName = dbName;}

void BinderHandler::SetLogin(const char *login)
{this->login = login;}

void BinderHandler::SetPWD(const char *pwd)
{this->pwd = pwd;}


void BinderHandler::FillTableView(QTableView *TableView, const char *ViewName, QWidget *wid)
{
    int nFields;
    int nTuples;

    DBConnect();

    if (!isConnectionOk())
    {
        ShowErrWithConnection();
    }else
    {
        QString strCmd;
        strCmd += "SELECT * FROM ";
        strCmd += ViewName;
        DBResultPointer = PQexec(DBConnPointer, QStrToCStr(strCmd));
        if ( PQresultStatus(DBResultPointer) != PGRES_TUPLES_OK)
            ShowErrCmdStatusBox();
        else
        {
            nFields = PQnfields(DBResultPointer);
            nTuples = PQntuples(DBResultPointer);

            QStandardItemModel *model = new QStandardItemModel(nTuples,nFields, wid);

            for (int i = 0; i < nFields; i++)
                model->setHorizontalHeaderItem(i, new QStandardItem(PQfname(DBResultPointer, i)));

            for (int i = 0; i < nTuples; i++)
            {
                for (int j = 0; j < nFields; j++)
                    model->setItem(i, j, new QStandardItem(PQgetvalue(DBResultPointer, i, j)));
            }
            TableView->setModel(model);
        }
    }

    DBDisconnect();
}

void BinderHandler::FillListView(QListView *ListView, const char *ViewName, const char *ColumnName)
{
    int nTuples;

    DBConnect();

    if (!isConnectionOk())
    {
        ShowErrWithConnection();
    }else
    {
        QString strCmd;
        strCmd += "SELECT \"";
        strCmd += ColumnName;
        strCmd += "\" FROM ";
        strCmd += ViewName;

        DBResultPointer = PQexec(DBConnPointer, QStrToCStr(strCmd));
        if ( PQresultStatus(DBResultPointer) != PGRES_TUPLES_OK)
            ShowErrCmdStatusBox();
        else
        {
            nTuples = PQntuples(DBResultPointer);

            QStringListModel *model = new QStringListModel();
            QStringList list;

            for (int i = 0; i < nTuples; i++)
                list << PQgetvalue(DBResultPointer, i, 0);

            model->setStringList(list);

            ListView->setModel(model);
        }
    }

    DBDisconnect();
}

QString BinderHandler::getValueFromCmd(const char *Cmd)
{
    DBConnect();

    QString vresult;

    if (!isConnectionOk())
    {
        ShowErrWithConnection();
    }else
    {
        QString strCmd;
        strCmd += "SELECT * FROM ";
        strCmd += Cmd;

        DBResultPointer = PQexec(DBConnPointer, QStrToCStr(strCmd));
        if ( PQresultStatus(DBResultPointer) != PGRES_TUPLES_OK)
            ShowErrCmdStatusBox();
        else
            vresult += PQgetvalue(DBResultPointer, 0, 0);
    }

    DBDisconnect();
    return vresult;
}
void BinderHandler::updateStatus(int id, int newprio)
{
    DBConnect();

    if (!isConnectionOk())
    {
        ShowErrWithConnection();
    }else
    {
        QString strCmd="";
        strCmd += "SELECT zmien_status_zamowienia('";
        strCmd += QString::number(id) + "', '";
        strCmd += QString::number(newprio) + "')";

        DBResultPointer = PQexec(DBConnPointer, QStrToCStr(strCmd));
        if ( PQresultStatus(DBResultPointer) != PGRES_TUPLES_OK)
            ShowErrCmdStatusBox();
    }

    DBDisconnect();
}
void BinderHandler::updateilosc(int id, int amount)
{
    DBConnect();

    if (!isConnectionOk())
    {
        ShowErrWithConnection();
    }else
    {
        QString strCmd="";
        strCmd += "SELECT zmien_liczbe_produktow('";
        strCmd += QString::number(id) + "', '";
        strCmd += QString::number(amount) + "')";

        DBResultPointer = PQexec(DBConnPointer, QStrToCStr(strCmd));
        if ( PQresultStatus(DBResultPointer) != PGRES_TUPLES_OK)
            ShowErrCmdStatusBox();
    }

    DBDisconnect();
}



void BinderHandler::deleteEmployee(const char* imie, const char* nazwisko, const char* mail)
{
    DBConnect();
    if (!isConnectionOk())
    {
        ShowErrWithConnection();
    }else
    {
        QString strCmd;
        strCmd += "SELECT usun_pracownika('";
        strCmd += imie;
        strCmd += "', '";
        strCmd += nazwisko;
        strCmd += "', '";
        strCmd += mail;
        strCmd +="');";
        DBResultPointer = PQexec(DBConnPointer, QStrToCStr(strCmd));
        if ( PQresultStatus(DBResultPointer) != PGRES_TUPLES_OK)
            ShowErrCmdStatusBox();
    }
    DBDisconnect();
}

// Przeładowanie, przykrywka
void BinderHandler::deleteEmployee(QString imie, QString nazwisko, QString mail)
{
    deleteEmployee(QStrToCStr(imie), QStrToCStr(nazwisko), QStrToCStr(mail));
}

void BinderHandler::addEmployee(const char *Imie, const char *Nazwisko,
                                const char *Placa, int idStanowiska, const char *Ulica,
                                int NumerMieszkania, int NumerDomu, const char *Miejscowosc,
                                int KodPocztowy, int NumerTel, const char *Mail,
                                int NrKadry, const char *Data)
{
    DBConnect();
    if (!isConnectionOk())
    {
        QMessageBox msgBox;
        msgBox.setText("Error: Blad polaczenia z serverem.\n");
        msgBox.exec();
    }else
    {
        QString strCmd;
        strCmd += "SELECT dodaj_pracownika('";
        strCmd += Imie;                   strCmd += "', '";
        strCmd += Nazwisko;               strCmd += "', to_number('";
        strCmd += Placa;                  strCmd += "', ''), '";
        strCmd += IToQS(idStanowiska);    strCmd += "', '";
        strCmd += Ulica;                  strCmd += "', ";
        strCmd += IToQS(NumerMieszkania); strCmd += ", ";
        strCmd += IToQS(NumerDomu);       strCmd += ", '";
        strCmd += Miejscowosc;            strCmd += "', ";
        strCmd += IToQS(KodPocztowy);     strCmd += ", ";
        strCmd += IToQS(NumerTel);        strCmd += ", '";
        strCmd += Mail;                   strCmd += "', ";
        strCmd += IToQS(NrKadry);
        if (Data == NULL )
        {
            strCmd += ");";
        }
        else
        {
            strCmd += ", ";
            strCmd += "to_date('";
            strCmd += Data;
            strCmd += "', 'YYYY-MM-DD'));";
        }
        //ShowInfoBox(strCmd); // Tylko dla DEBUGU !
        DBResultPointer = PQexec(DBConnPointer, QStrToCStr(strCmd));
        if ( PQresultStatus(DBResultPointer) != PGRES_TUPLES_OK)
            ShowErrCmdStatusBox();
    }
    DBDisconnect();
}

void BinderHandler::addEmployee(QString Imie, QString Nazwisko, int Placa,
                                int idStanowiska, QString Ulica, int NumerMieszkania,
                                int NumerDomu, QString Miejscowosc, int KodPocztowy,
                                int NumerTel, QString Mail, int NrKadry, QString Data)
{
    addEmployee(QStrToCStr(Imie),
                QStrToCStr(Nazwisko),
                Placa,
                idStanowiska,
                QStrToCStr(Ulica),
                NumerMieszkania,
                NumerDomu,
                QStrToCStr(Miejscowosc),
                KodPocztowy,
                NumerTel,
                QStrToCStr(Mail),
                NrKadry,
                QStrToCStr(Data)
                );
}

const char* BinderHandler::GetStrFromTblItem(QTableView *qTableP, int column, int row)
{
    return qTableP->model()->index(row,column).data().toString().toStdString().c_str();
}

void BinderHandler::updateRow(QString tableName, QString columnName, QString newValue, QString ID)
{
        DBConnect();
        if (!isConnectionOk())
        {
            QMessageBox msgBox;
            msgBox.setText("Error: Blad polaczenia z serverem.\n");
            msgBox.exec();
        }else
        {
            QString strCmd;
            strCmd += "";
            strCmd += "UPDATE ";
            strCmd += "schemat.";
            strCmd += tableName;
            strCmd += " SET \"";
            strCmd += columnName;
            strCmd += "\" = ";
            strCmd += newValue;
            strCmd += " WHERE id = ";
            strCmd += ID;
            strCmd += ";";
            // DEBUG !!
            // ShowInfoBox(strCmd);
            DBResultPointer = PQexec(DBConnPointer, QStrToCStr(strCmd));
            if ( PQresultStatus(DBResultPointer) != PGRES_COMMAND_OK)
                ShowErrCmdStatusBox();
        }
        DBDisconnect();
}

void BinderHandler::updateStringVal(QString tableName, QString columnName, QString newValue, int ID)
{
    updateRow(tableName, columnName,"'" + newValue + "'", IToQS(ID));
}

void BinderHandler::updateDateVal(QString tableName, QString columnName, QString newValue,  int ID, QString dateFormat)
{
    QString strCmd;
    strCmd = "";
    strCmd += " to_date('";
    strCmd += newValue;
    strCmd += "', '";
    strCmd +=dateFormat;
    strCmd +="')";
    updateRow(tableName, columnName, strCmd, IToQS(ID));
}

void BinderHandler::updateNumericVal(QString tableName, QString columnName, QString newValue, int ID)
{
    QString strCmd;
    strCmd += " to_number('";
    strCmd += newValue;
    strCmd += "', '')";
    updateRow(tableName, columnName, strCmd, IToQS(ID));
}

void BinderHandler::updateNumericVal(QString tableName, QString columnName, double newValue, int ID)
{
    QString strCmd;
    strCmd += " to_number('";
    strCmd += DToQS(newValue);
    strCmd += "', '')";
    updateRow(tableName, columnName, strCmd, IToQS (ID));
}

void BinderHandler::updateIntVal(QString tableName, QString columnName, QString newValue, int ID)
{
    updateRow(tableName, columnName, newValue, IToQS(ID));
}

void BinderHandler::updateIntVal(QString tableName, QString columnName, int newValue, int ID)
{
    updateRow(tableName, columnName, IToQS(ID), IToQS(ID));
}

//================================================
//================================================
//================================================
