#include <iostream>
#include <iomanip>
#include <libpq-fe.h>
#include "lib\sqlconlib.h"

using namespace std;

//dane do połączenia
char pghost[] = "localhost";
char pgport[] = "5432";
char pgbase[] = "baza";
char pgnew_base[] = "nowa.sql";
char pguser[] = "postgres";
char pgpass[] = "postgres";
char szkielet[] = "szkielet_bazy.sql";
char widoki[] = "widoki.sql";
char tiggery[] = "tiggery.sql";
char funkcje[] = "funkcje.sql";
char dane[] = "dane_poczatkowe.sql";


int main(int argc, char* argv[])
{

        int nFields;
        int width = 10;  // szerokosc widoku

        PGconn *conn;
        PGresult *result;

        conn = PQsetdbLogin(pghost,pgport,NULL,NULL,NULL,pguser,pgpass);

        if (PQstatus(conn) == CONNECTION_OK)
        {
            cout << "Pomyslnie polaczono z serverem \n";
        }
        else
        {
            cout << "Error: Blad polaczenia z serverem %s\n";
            PQfinish(conn);
        }

        result = PQexec(conn, "select * from pokaz_klientow");

        nFields = PQnfields(result);

        for (int i = 0; i < nFields; i++)
                cout << left << skipws << setw(width) << PQfname(result, i) << " ";
        cout << "\n\n";

        /* next, print out the rows */
        for (int i = 0; i < PQntuples(result); i++)
        {
                for (int j = 0; j < nFields; j++)
                        cout << left << skipws << setw(width) << PQgetvalue(result, i, j) << " ";
                cout << "\n";
        }

        PQfinish(conn);

        getchar();

        return 0;
}
