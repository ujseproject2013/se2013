#-------------------------------------------------
#
# Project created by QtCreator 2013-05-04T02:31:04
#
#-------------------------------------------------

QT       += core gui

INCLUDEPATH += C:/pqsql/include
LIBS += C:/pqsql/lib/libpq.lib

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = LibTesterQT5
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    ../../../src/gui-binder.cpp

HEADERS  += mainwindow.h \
    ../../../src/gui-binder.h

HEADERS  += libpq-fe.h

FORMS    += mainwindow.ui

