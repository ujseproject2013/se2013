#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QMessageBox>
#include <QAbstractItemModel>
#include <QStandardItemModel>

#include "libpq-fe.h"
#include "../../../src/gui-binder.h"

char pghost[] = "localhost";
char pgport[] = "5432";
char pguser[] = "postgres";
char pgpass[] = "postgres";

int selectedRow = -2;



MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);



    //DbConnect(ui->tableView);

}

MainWindow::~MainWindow()
{
    QMessageBox exitMsg;
    exitMsg.setText("Do Widzenia !");
    exitMsg.exec();
    delete ui;
}

void MainWindow::on_refreshButt_clicked()
{
    //----------------------------------
    //  Test metod łączących się z bazą
    //----------------------------------

    //Ustalanie parametrów połączenia
    BHandler.SetDBConnStrings(pghost,pgport,NULL,NULL,"baza1",pguser,pgpass);

    //Wypełnij tabelę
    BHandler.FillTableView(ui->tableView, "pracownik");

    //Ukryj ID'eki pracowników
    ui->tableView->setColumnHidden(0, true);

    //Wypełnij listę
    BHandler.FillListView(ui->listView, "pokaz_klientow", "Imie");

    //Ustaw licznik lcdNumber na wartość z zapytania (widok liczba_aktywnych_pracownikow)
    //Sfotmatuj wynik QString->toInt
    ui->lcdNumber->display(BHandler.getValueFromCmd("liczba_aktywnych_pracownikow").toInt());

    //Wypelnij textEdit wartoscia z widoku max_pensja,
    //zwraca typ numeryczny, w tym przypadku kwotę
    ui->textEdit->clear();
    ui->textEdit->appendPlainText(BHandler.getValueFromCmd("max_pensja"));

}

void MainWindow::on_delButt_clicked()
{
    if (selectedRow != -1)
    {
        // Wyciaganie konkretnych pol z QTableView
        QString imie = BHandler.GetStrFromTblItem(ui->tableView, 1, selectedRow);
        QString nazwisko = BHandler.GetStrFromTblItem(ui->tableView, 2, selectedRow);
        QString mail = BHandler.GetStrFromTblItem(ui->tableView, 10, selectedRow);

        // MessageBox potwierdzajacy usuniecie
        QMessageBox msgBox;
        msgBox.setIcon(QMessageBox::Question);
        msgBox.setStandardButtons(QMessageBox::Ok | QMessageBox::Cancel);
        msgBox.setText("Czy na pewno chcesz usunąć " + imie + "?");

        // Aktywacja msgBox, sprawdz czy uzytkownik kliknął "Ok"
        if(QMessageBox::Ok == msgBox.exec())
        {
            //Funkcja usuniecia pracownika
            BHandler.deleteEmployee(imie.toStdString().c_str(),
                                    nazwisko.toStdString().c_str(),
                                    mail.toStdString().c_str());
            BHandler.FillTableView(ui->tableView, "pracownik");
        }
    }
}

void MainWindow::on_tableView_clicked(const QModelIndex &index)
{
    ui->lcdNumber_2->display(ui->tableView->currentIndex().column());
    ui->lcdNumber_3->display(ui->tableView->currentIndex().row());
    selectedRow = index.row();
}

void MainWindow::on_pushButton_clicked()
{
    //Dodaj Pracownika
    BHandler.addEmployee("Franek", "Głośny", "2300", 1, "Metalowcóww", 13, 23,
                         "Kraków",32075, 666666666, "example34@mail.com", 1);

    //Uzupełnij tabele widokiem "simple_pracownik" ( SELECT * FROM simple_pracownik; )
    BHandler.FillTableView(ui->tableView, "pracownik");
}

void MainWindow::on_pushButton_2_clicked()
{
    ShowInfoBox(IToQS(23));
}

void MainWindow::on_confirmButt_clicked()
{
    BHandler.updateStringVal("pracownicy", "imie", "Boguś", 2);
    BHandler.updateDateVal("pracownicy", "koniec_urlopu", "1990-01-23", 2);
    BHandler.updateNumericVal("pracownicy", "pensja", "2300", 2);
//    BHandler.updateIntVal();

}
