#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "../../../src/gui-binder.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT
    
public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
    
private slots:
    void on_refreshButt_clicked();

    void on_MainWindow_customContextMenuRequested(const QPoint &pos);

    void on_listView_clicked(const QModelIndex &index);

    void on_delButt_clicked();

    void on_tableView_clicked(const QModelIndex &index);

    void on_pushButton_clicked();

    void on_pushButton_2_clicked();

    void on_confirmButt_clicked();

private:
    BinderHandler BHandler;
    Ui::MainWindow *ui;
};

#endif // MAINWINDOW_H
